//*****************************************************************************
//
// CONFIGURAÇÃO DE TESTE (J,K,L -> ENTRADA; M, N, P -> SAÍDAS)
// STATUS E ERRO (PORTA Q)
//*****************************************************************************

#include "clp.h"
//#define FCPU 120000000
//SAIDAS DO MICRO, DIREÇÃO DA PORTA 5, 7 E 8, 2.
#define S1 BIT0
#define S2 BIT1
#define S3 BIT2
#define S4 BIT3
#define S5 BIT4
#define S6 BIT5
#define S7 BIT6
#define S8 BIT7
#define S9 BIT0
#define S10 BIT1
#define ERRO BIT6
#define STATUS BIT7
#define LDAC BIT0

//ENTRADAS DO MICRO, DIREÇÃO DA PORTA 1 E 6.
#define E1 BIT2
#define E2 BIT1
#define E3 BIT0
#define E4 BIT7
#define E5 BIT6
#define E6 BIT5
#define E7 BIT4
#define E8 BIT5
#define E9 BIT4
#define E10 BIT4

uint16_t vetorDAC[4] = {0,0,0,0};

unsigned long long int Counter_ms=0, i=0, j=0, tmp = 46;
uint16_t i_Instruction = 0;
char I[33];
char X[33];
char Y[33];
char z[33];
char Q[33];
//Rising pulse indicator: default is false
char Maux[33];
char Qaux[33];
char Xaux[33];
char Yaux[33];
char zaux[33];
//
char M[33];
char T[33];
char R[33];
struct Timer v_Timer[33];
struct Stepper v_Stepper[8];
struct Instruction v_Instruction[120];
struct Coil v_Coil[120];
struct Contact v_Contact[360];
uint16_t Stepper_Instructions[10];
uint16_t aux_Stepper_Inst = 0, aux_Stepper = 0;
uint16_t aux1, aux2, aux3, aux4;
unsigned char *F_I, *F_X, *F_Y, *F_z, *F_Q, *F_M, *F_R, *F_T, *F_TVALUE;

char FrameTx[20];
char FrameRx[20];
char contFrame = 0;
bool isLoopCycleFinished;
bool isFrameReady = false;
bool isFirstLoop = true;
bool is_5ms_loop = true;
bool is_us_loop = true;
bool RoutineStarted = false;
bool estado = false;
uint32_t count_us = 0;
uint32_t last_count_us = 0;
uint32_t aux_count_us = 0;
uint32_t count_ms = 0;
uint32_t last_count_ms = 0;
uint32_t ProductCounter = 0, aux_ProductCounter = 0;
uint16_t aux_value = 0, aux_value2 = 0, aux_value3 = 0;

unsigned char aux_GetConfigs = 0;
uint16_t tmp_index = 29;

uint16_t aux_Contact = 0;
uint16_t aux_Coil = 0;
uint16_t aux_Inst = 0;

uint16_t tempos[100], index_tempos = 0, aux_tempo = 0;

uint16_t teste[100];

uint32_t tmp_timer1 = 0;
uint32_t tmp_timer2 = 0;
uint32_t tmp_timer3 = 0;
uint32_t tmp_rx = 0;

//unsigned char estado_f = PARAR;
unsigned char modificar_estado = 0;
unsigned char gravou = 1;//proteção para realizar gravação apenas após finalização de um ciclo válido de operação
unsigned int timeout = 0;//timeout padrão
unsigned char ligar = 0, sincronismo = 0;
unsigned int enviar_contador = 0;
unsigned int cont_filme_parado = 0, h_cont_filme_parado = 0;

unsigned int velocidade_esteira = 1600;
unsigned int velocidade_filme = 352;
unsigned int ppm = 110;
unsigned int tamanho = 174;


unsigned int periodo_esteira = 0, periodo_filme = 0, cont_esteira = 0, cont_filme = 0, cont_aux_esteira = 0, cont_aux_filme = 0, cont_aux2 = 0;

unsigned char alterou_velocidade = 0, flag_aumentar_velocidade = 0, flag_diminuir_velocidade = 0, limitetransicao = 1;

unsigned int inc_cont_geral = 0, aux_inc_cont_geral = 0;
unsigned int aux_cont_ligar = 0, contaux = 0;

unsigned char filme_sem_tarja = 0, esteira_sem_sensor = 0;
unsigned int sensor_esteira_ativo = 0, sensor_filme_ativo = 0, control_esteira = 0, control_filme = 0;//armazena estado do sensor para evitar variações bruscas

unsigned char chegou_filme = 0, chegou_esteira = 0, primeira_rodada = 1, primeira_batida = 1;
unsigned char tratamento_pe_ativo = 0, tratamento_pf_ativo = 0;
unsigned char aumentar_velocidade_filme_depois = 0, diminuir_velocidade_filme_depois = 0;

unsigned int periodos_esteira[50], periodos_filme[50],
             indice_p_esteira = 0, indice_p_filme = 0;
unsigned int velocidades_filme[50], indice_vel_filme = 0;
unsigned int diff_ef[50], indice_diff_ef = 0;

unsigned int wait_for_DAC = 0;
unsigned int flag_aumentar = 0;
unsigned int flag_diminuir = 0;

//Novas funções para o TM4C1924
    void Flash_rw(uint32_t segmento, char byte); //OK
    void configureUart(uint32_t clock, uint32_t baud); //OK
    void DAC_Init(uint32_t clock);
    void DAC_Write(unsigned char channel, uint16_t value);
    uint16_t DAC_Read(unsigned char channel);
    bool BL_Available(void);
    uint16_t BL_Read(uint8_t *vetor, uint16_t length);
    void BL_Write(uint8_t *vetor, uint16_t length);

    void ManageTimer();
    void Start(); //OK
    void OperationCycle(); //OK
    bool F_Coil(struct Instruction *Inst, unsigned int Index); //OK
    bool F_Timer(struct Instruction *Inst, unsigned int Index_Inst); //OK
    bool F_Stepper(struct Instruction *Inst, unsigned int Index_Inst); //OK
    void SetContact(unsigned int Index, unsigned char Vector, unsigned char VectorIndex, unsigned char isNO); //OK
    void SetCoil(unsigned int Index, unsigned char Vector, unsigned char Type, unsigned char VectorIndex); //OK
    void SetTimer(unsigned int Index, unsigned char Mode, unsigned char Base, unsigned int SetValue,
                  unsigned char Type, unsigned char ResetVector, unsigned char ResetIndex); //OK
    void SetStepper(unsigned int Index, unsigned char Mode, unsigned char Base,
                    unsigned int Speed, unsigned int SetAccValue, unsigned int SetTotalValue,
                    unsigned int SetDecValue, unsigned char Vector, unsigned char Vector_Index); //OK
    void SetInstruction(unsigned int Index, unsigned char Func_Type, unsigned int Func_Index, bool HasOrInstruction,
                        struct Contact Contact_1, struct Contact Contact_2, struct Contact Contact_3); //OK
    void SetNewCoilInstruction(unsigned char Vector_1, unsigned char VectorIndex_1, unsigned char isNO_1,
                               unsigned char Vector_2, unsigned char VectorIndex_2, unsigned char isNO_2,
                               unsigned char Vector_3, unsigned char VectorIndex_3, unsigned char isNO_3,
                               unsigned char Vector_Coil, unsigned char VectorIndex_Coil, unsigned char Type_Coil,
                               bool HasOrInstruction); //OK

    void SetNewTimerInstruction(unsigned char Vector_1, unsigned char VectorIndex_1, unsigned char isNO_1,
                                   unsigned char Vector_2, unsigned char VectorIndex_2, unsigned char isNO_2,
                                   unsigned char Vector_3, unsigned char VectorIndex_3, unsigned char isNO_3,
                                   unsigned char Timer_Index, unsigned char Mode, unsigned char Base,
                                   unsigned int SetValue, unsigned char Type, unsigned char ResetVector,
                                   unsigned char ResetIndex, bool HasOrInstruction); //OK

    void SetNewStepperInstruction(unsigned char Vector_1, unsigned char VectorIndex_1, unsigned char isNO_1,
                                  unsigned char Vector_2, unsigned char VectorIndex_2, unsigned char isNO_2,
                                  unsigned char Vector_3, unsigned char VectorIndex_3, unsigned char isNO_3,
                                  unsigned char Stepper_Index, unsigned char Base, unsigned int Speed,
                                  unsigned int SetAccValue, unsigned int SetTotalValue, unsigned int SetDecValue,
                                  unsigned char Vector, unsigned char Vector_Index, bool HasOrInstruction); //OK
    bool GetInstructionResult(struct Instruction *Inst); //OK
    bool GetContactState(struct Contact *Contact_1); //OK
    void ProcessFrame(); //OK
    void ProcessGetConfigs(); //OK
    void ProcessGetSpeed(); //OK
    void ProcessGetLength(); //OK
    void ProcessReturn(); //OK
    void ProcessGetCounter(); //OK
    void ProcessGetPowerState(); //OK
    void ProcessGetSensorState(); //OK
    void ProcessGetTemperatureState(); //OK
    void ProcessGetStampState(); //OK
    void ProcessWriteSingleRegister(); //OK
    void ProcessWriteSingleRegisterTimer(); //OK
    void sendTx(unsigned char size); //OK
    void CodigoLadder();
    void CodigoTeste();
    void Fail_LoopCycleTimeout(); //OK
    void Flash_wb(uint32_t ui32Address, char byte); //OK
    void clear_Seg(uint32_t segmento); //OK

    unsigned char checksum(unsigned char *frame, unsigned char length); //OK

    void IncrementaContadores();


void IncrementaContadores(){
    aux_inc_cont_geral = inc_cont_geral;
    inc_cont_geral = 0;
    aux_cont_ligar += aux_inc_cont_geral;
    control_esteira+= aux_inc_cont_geral;
    control_filme+= aux_inc_cont_geral;
    timeout+= aux_inc_cont_geral;
    contaux+= aux_inc_cont_geral;
    cont_aux2+= aux_inc_cont_geral;
    cont_filme_parado+= aux_inc_cont_geral;
}

bool BL_Available()
{
    return UARTCharsAvail(UARTX_BASE);
}

uint16_t BL_Read(uint8_t *vetor, uint16_t length)
{
    uint16_t aux = 0;
    uint16_t count = 0;

    for(aux=0; aux<length; aux++)
    {
      if(BL_Available())
      {
        vetor[aux] = UARTCharGetNonBlocking(UARTX_BASE);
        count++;
      }
      delay_us(100);
    }

    return count;
}

void BL_Write(uint8_t *vetor, uint16_t length)
{
    uint16_t aux = 0;
    for(aux=0; aux<length; aux++)
    {
      UARTCharPutNonBlocking(UARTX_BASE, vetor[aux]);
    }
}

void delay_us(uint32_t us)
{
    SysCtlDelay((FCPU/(3*1000000)*us));
}

void DAC_Init(uint32_t clock)
{
    POUT(PQ, LDAC, 0);

    SysCtlPeripheralEnable(SYSCTL_PERIPH_I2C1);
    SysCtlDelay(100);
    SysCtlPeripheralReset(SYSCTL_PERIPH_I2C1);

    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOG);
    SysCtlDelay(100);

    GPIOPinConfigure(GPIO_PG0_I2C1SCL);
    GPIOPinConfigure(GPIO_PG1_I2C1SDA);


    GPIOPinTypeI2CSCL(GPIO_PORTG_BASE, GPIO_PIN_0);
    GPIOPinTypeI2C(GPIO_PORTG_BASE, GPIO_PIN_1);

    I2CMasterInitExpClk(I2C1_BASE, clock, false);//false for 100kHz mode
    I2CMasterEnable(I2C1_BASE);

    //I2CMasterGlitchFilterConfigSet(I2C1_BASE, I2C_MASTER_GLITCH_FILTER_DISABLED);

    delay_us(100);

    //INICIA VREF = VDD
    I2CMasterSlaveAddrSet (I2C1_BASE, 0x60, false); //Write
    I2CMasterDataPut (I2C1_BASE, (0x80) );
    I2CMasterControl (I2C1_BASE, I2C_MASTER_CMD_SINGLE_SEND);
    while (I2CMasterBusy(I2C1_BASE)){}
    if(I2CMasterErr(I2C1_BASE) != 0) return;

    delay_us(1000);

    //INICIA GANHO UNITÁRIO
    /*I2CMasterSlaveAddrSet (I2C1_BASE, 0x60, false); //Write
    I2CMasterDataPut (I2C1_BASE, (0xC0) );
    I2CMasterControl (I2C1_BASE, I2C_MASTER_CMD_SINGLE_SEND);
    while (I2CMasterBusy(I2C1_BASE)){}
    if(I2CMasterErr(I2C1_BASE) != 0) return;*/

    delay_us(1000);

    //INICIA PD0/PD1
    I2CMasterSlaveAddrSet (I2C1_BASE, 0x60, false); //Write
    I2CMasterDataPut (I2C1_BASE, (0xA0) );
    I2CMasterControl (I2C1_BASE, I2C_MASTER_CMD_BURST_SEND_START);
    while (I2CMasterBusy(I2C1_BASE)){}
    if(I2CMasterErr(I2C1_BASE) != 0) return;
    delay_us(1000);
    I2CMasterDataPut (I2C1_BASE, (0x00) );
    I2CMasterControl (I2C1_BASE, I2C_MASTER_CMD_BURST_SEND_FINISH);
    while (I2CMasterBusy(I2C1_BASE)){}
    if(I2CMasterErr(I2C1_BASE) != 0) return;

    delay_us(1000);

}

void DAC_Write(uint8_t channel, uint16_t value)
{
    POUT(PQ, LDAC, 0);

    if (channel > 3) channel = 3;
    if (value > 4095) value = 4095;

    vetorDAC[channel] = value;

    delay_us(1000);

    I2CMasterSlaveAddrSet (I2C1_BASE, 0x60, false); //Write
    I2CMasterDataPut (I2C1_BASE,  ((0x58) | (channel<<1)));
    I2CMasterControl (I2C1_BASE, I2C_MASTER_CMD_BURST_SEND_START);
    while (I2CMasterBusy(I2C1_BASE)){}
    if(I2CMasterErr(I2C1_BASE) != 0) return;
    delay_us(1000);
    I2CMasterDataPut (I2C1_BASE,  (vetorDAC[channel]>>8));
    I2CMasterControl (I2C1_BASE, I2C_MASTER_CMD_BURST_SEND_CONT);
    while (I2CMasterBusy(I2C1_BASE)){}
    if(I2CMasterErr(I2C1_BASE) != 0) return;
    delay_us(1000);
    I2CMasterDataPut (I2C1_BASE,  (vetorDAC[channel]));
    I2CMasterControl (I2C1_BASE, I2C_MASTER_CMD_BURST_SEND_FINISH);
    while (I2CMasterBusy(I2C1_BASE)){}
    if(I2CMasterErr(I2C1_BASE) != 0) return;
    delay_us(1000);
    delay_us(100000);
    POUT(PQ, LDAC, 0);


}

uint16_t DAC_Read(unsigned char channel)
{
  POUT(PQ, LDAC, 0);

  if (channel > 3) channel = 3;

  ///READ
  I2CMasterSlaveAddrSet (I2C1_BASE, 0x60, true);

  I2CMasterControl (I2C1_BASE, I2C_MASTER_CMD_BURST_RECEIVE_START);
  while (I2CMasterBusy(I2C1_BASE)){}
  delay_us(1000);
  if((uint8_t) I2CMasterErr(I2C1_BASE) == 0) I2CMasterDataGet(I2C1_BASE);

  uint8_t aux = 0;
  uint8_t aux2 = 0;
  uint8_t aux3 = 0;
  for (aux=0; aux<=3; aux++)
  {
    for (aux2=0; aux2<=5; aux2++)
    {
        I2CMasterControl (I2C1_BASE, I2C_MASTER_CMD_BURST_RECEIVE_CONT);
        while (I2CMasterBusy(I2C1_BASE)){}
        delay_us(1000);
        if((uint8_t) I2CMasterErr(I2C1_BASE) == 0)
        {
            UARTCharPutNonBlocking(UARTX_BASE, I2CMasterDataGet(I2C1_BASE));
        }
    }
  }

  I2CMasterControl (I2C1_BASE, I2C_MASTER_CMD_BURST_RECEIVE_FINISH); //Send
  while (I2CMasterBusy(I2C1_BASE)){} //Wait till end of transaction
  delay_us(1000);

  return vetorDAC[channel];

}


void atualizaentradas(){

     //   P8OUT^=BIT0;
      if(PIN(PE, E1))
          I[1]=false;
      else
          I[1]=true;

      if(PIN(PE, E2))
          I[2]=false;
      else
          I[2]=true;

      if(PIN(PE, E3))
          I[3]=false;
      else
          I[3]=true;

      if(PIN(PD, E4))
          I[4]=false;
      else
          I[4]=true;

      if(PIN(PD, E5))
          I[5]=false;
      else
          I[5]=true;

      if(PIN(PD, E6))
          I[6]=false;
      else
          I[6]=true;

      if(PIN(PD, E7))
          I[7]=false;
      else
          I[7]=true;

      if(PIN(PE, E8))
          I[8]=false;
      else
          I[8]=true;

      if(PIN(PE, E9))
          I[9]=false;
      else
          I[9]=true;

      if(PIN(PB, E10))
          I[10]=false;
      else
          I[10]=true;

}

void atualizasaidas(int ind, bool res){
    Q[ind] = (res ? 1 : 0);
    switch(ind){
    case 1:
        if (res)
            POUT(PM, S1, 0);
        else
            POUT(PM, S1, S1);
        break;
    case 2:
        if (res)
            POUT(PM, S2, 0);
        else
            POUT(PM, S2, S2);
        break;
    case 3:
        if (res)
            POUT(PM, S3, 0);
        else
            POUT(PM, S3, S3);
        break;
    case 4:
        if (res)
            POUT(PM, S4, 0);
        else
            POUT(PM, S4, S4);
        break;
    case 5:
        if (res)
            POUT(PM, S5, 0);
        else
            POUT(PM, S5, S5);
        break;
    case 6:
        if (res)
            POUT(PM, S6, 0);
        else
            POUT(PM, S6, S6);
        break;
    case 7:
        if (res)
            POUT(PM, S7, 0);
        else
            POUT(PM, S7, S7);
        break;
    case 8:
        if (res)
            POUT(PM, S8, 0);
        else
            POUT(PM, S8, S8);
        break;
    case 9:
        if (res)
            POUT(PP, S9, 0);
        else
            POUT(PP, S9, S9);
        break;
    case 10:
        if (res)
            POUT(PP, S10, 0);
        else
            POUT(PP, S10, S10);
        break;
    }
}

void OperationCycle()
{
//      IE2 &= ~UCA0RXIE;                          // disable USCI_A0 RX interrupt
    //Verify Cycle Timeout
    if (is_5ms_loop)
    {
        is_5ms_loop = false;
        if (!isLoopCycleFinished)
        {
//            Fail_LoopCycleTimeout();
//            return;
        }
        isLoopCycleFinished = false;
        atualizaentradas(); //Atualizar Entradas     //****************CRIAR FUNÃ‡ÃƒO PARA AS ENTRADAS *******    OK
        //Realizar comunicaÃƒÂ§ÃƒÂ£o

        if (isFirstLoop)
        {
            while (i_Instruction < aux_Inst)
            {
                switch(v_Instruction[i_Instruction].Func_Type)
                {
                case FUNC_COIL:
                    i_Instruction += F_Coil(&(v_Instruction[i_Instruction]), i_Instruction);
                    break;
                case FUNC_TIMER:
                    i_Instruction += F_Timer(&(v_Instruction[i_Instruction]), i_Instruction);
                    break;
                case FUNC_STEPPER:
                    i_Instruction += F_Stepper(&(v_Instruction[i_Instruction]), i_Instruction);
                    break;
                }
                i_Instruction=i_Instruction+1;
            }
            isFirstLoop = false;
        }
        i_Instruction = 0;

        while (i_Instruction < aux_Inst)
        {
            if (i_Instruction == tmp)
                tmp = i_Instruction;
            switch(v_Instruction[i_Instruction].Func_Type)
            {
            case FUNC_COIL:
                i_Instruction += F_Coil(&(v_Instruction[i_Instruction]), i_Instruction);
                break;
            case FUNC_TIMER:
                i_Instruction = i_Instruction + F_Timer(&(v_Instruction[i_Instruction]), i_Instruction);
                break;
            case FUNC_STEPPER:
                i_Instruction = i_Instruction + F_Stepper(&(v_Instruction[i_Instruction]), i_Instruction);
                break;
            }
            i_Instruction=i_Instruction+1;
        }

        if (isFrameReady == true)
            ProcessFrame();

        if (aux_GetConfigs != 0 && Counter_ms%50 == 0)
        {
            ProcessGetConfigs();
        }
        isLoopCycleFinished = true;
        if (Counter_ms == 50000)
            Counter_ms = 1;
//            else
//                Counter_ms++;
    }
    else
    {
        aux_Stepper = 0;
        i_Instruction = Stepper_Instructions[aux_Stepper++];
        while (i_Instruction > 0)
        {
            F_Stepper(&(v_Instruction[i_Instruction]), i_Instruction);
            i_Instruction = Stepper_Instructions[aux_Stepper++];
        }
    }
}

bool GetInstructionResult(struct Instruction *Inst)
{
    return (GetContactState(&(*Inst).Inst_Contact_1) && GetContactState(&(*Inst).Inst_Contact_2) && GetContactState(&(*Inst).Inst_Contact_3));
}

bool GetContactState(struct Contact *Contact_1)
{

    switch((*Contact_1).Vector)
    {
    case CONTACT_VECTOR_I:

        if ((*Contact_1).isNO == true)
            return I[(*Contact_1).Index];
        else
            return !I[(*Contact_1).Index];
        break;
    case CONTACT_VECTOR_X:

        if ((*Contact_1).isNO == true)
            return X[(*Contact_1).Index];
        else
            return !X[(*Contact_1).Index];
        break;
    case CONTACT_VECTOR_Y:

        if ((*Contact_1).isNO == true)
            return Y[(*Contact_1).Index];
        else
            return !Y[(*Contact_1).Index];
        break;
    case CONTACT_VECTOR_Z:

        if ((*Contact_1).isNO == true)
            return z[(*Contact_1).Index];
        else
            return !z[(*Contact_1).Index];
        break;
    case CONTACT_VECTOR_Q:

        if ((*Contact_1).isNO == true)
            return Q[(*Contact_1).Index];
        else
            return !Q[(*Contact_1).Index];
        break;
    case CONTACT_VECTOR_M:

        if ((*Contact_1).isNO == true)
            return M[(*Contact_1).Index];
        else
            return !M[(*Contact_1).Index];
        break;
    case CONTACT_VECTOR_T:

        if ((*Contact_1).isNO == true)
            return T[(*Contact_1).Index];
        else
            return !T[(*Contact_1).Index];

        break;
    case CONTACT_VECTOR_ZERO:

            return true;
        break;
    }
}

void Start()//DEVE ESTAR NO INÃƒÂ�CIO DO MAIN
{
    Counter_ms = 0;
    isLoopCycleFinished = true;

    for (i=0; i < 32; i++)
    {
        I[i]=0;
        X[i]=0;
        Y[i]=0;
        z[i]=0;
        M[i]=0;
        R[i]=0;
        Q[i]=0;
        Maux[i]=false;
        Qaux[i]=false;
        Xaux[i]=false;
        Yaux[i]=false;
        zaux[i]=false;

    }
    for (i=0; i < 10; i++)
       Stepper_Instructions[i] = 0;
    X[11]=1;

    F_I = FLASH_VECTOR_I;
    F_X = FLASH_VECTOR_X;
    F_Y = FLASH_VECTOR_Y;
    F_z = FLASH_VECTOR_Z;
    F_Q = FLASH_VECTOR_Q;
    F_M = FLASH_VECTOR_M;
    F_R = FLASH_VECTOR_R;
    F_T = FLASH_VECTOR_T;
    F_TVALUE = FLASH_VECTOR_TIMER;


    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 8; j++)
        {
            if (i < 4)
            {
                Y[i*8+j] = (*(F_Y+i)&(0x01<<j) ? true : false);
                R[8*i+j] = *(F_R+(8*i+j));
            }
            aux1 = F_TVALUE+2*(8*i+j);
            aux2 = *(F_TVALUE+2*(8*i+j));
            aux3 = F_TVALUE+2*(8*i+j)+1;
            aux4 = *(F_TVALUE+2*(8*i+j)+1);
            aux_value = (*(F_TVALUE+2*(8*i+j))<<8)|*(F_TVALUE+2*(8*i+j)+1);

        }
    }

    ppm =  (R[4]<<8)|R[5];
    tamanho = (R[6]<<8)|R[7];
    velocidade_esteira = ppm*35;
    velocidade_filme = (tamanho+6)*ppm*3/17;
    ProductCounter = (R[8]<<8)|R[9];

    RoutineStarted = true;
    ProcessGetSpeed();
    delay_us(200000);
    ProcessGetLength();
    delay_us(200000);


                FrameTx[9] = 0x22;
                FrameTx[10] = 0x01;
                FrameTx[11] = R[10];
                FrameTx[12] = R[11];
                ProcessReturn();
                delay_us(200000);
                FrameTx[9] = 0x22;
                FrameTx[10] = 0x02;
                FrameTx[11] = R[12];
                FrameTx[12] = R[13];
                ProcessReturn();
                delay_us(200000);
                FrameTx[9] = 0x22;
                FrameTx[10] = 0x03;
                FrameTx[11] = R[14];
                FrameTx[12] = R[15];
                ProcessReturn();
                delay_us(200000);
                FrameTx[9] = 0x22;
                FrameTx[10] = 0x04;
                FrameTx[11] = R[16];
                FrameTx[12] = R[17];
                ProcessReturn();
                delay_us(200000);
                FrameTx[9] = 0x22;
                FrameTx[10] = 0x05;
                FrameTx[11] = R[18];
                FrameTx[12] = R[19];
                ProcessReturn();
                delay_us(200000);
                FrameTx[9] = 0x22;
                FrameTx[10] = 0x06;
                FrameTx[11] = R[20];
                FrameTx[12] = R[21];
                ProcessReturn();
                delay_us(200000);
                FrameTx[9] = 0x22;
                FrameTx[10] = 0x07;
                FrameTx[11] = R[22];
                FrameTx[12] = R[23];
                ProcessReturn();
                delay_us(200000);
                FrameTx[9] = 0x22;
                FrameTx[10] = 0x08;
                FrameTx[11] = R[24];
                FrameTx[12] = R[25];
                ProcessReturn();
                delay_us(200000);
                FrameTx[9] = 0x22;
                FrameTx[10] = 0x09;
                FrameTx[11] = R[26];
                FrameTx[12] = R[27];
                ProcessReturn();



    CodigoLadder();//Tem que terminar de inicializar as variÃƒÂ¡veis
//        CodigoTeste();
}

bool F_Coil(struct Instruction *Inst, unsigned int Index)
{
    //P8OUT^=BIT0;
    bool aux_state = 0;
    unsigned int index = v_Coil[(*Inst).Func_Index].Index;
    bool Result = GetInstructionResult(Inst);
    unsigned int i = 0;
    if (Index + 1 < aux_Inst)
        while (v_Instruction[Index+i].Has_Or_Instruction)
        {
            i++;
            Result = Result||GetInstructionResult(&v_Instruction[Index+i]);
        }

    switch(v_Coil[(*Inst).Func_Index].Type)
    {
    //Common function
    case COIL_C:
        if (v_Coil[(*Inst).Func_Index].Vector == CONTACT_VECTOR_Q)
        {
            Q[index] = Result;
            atualizasaidas(index, Result);
        }
        else
            if (v_Coil[(*Inst).Func_Index].Vector == CONTACT_VECTOR_X)
            {
                X[index] = Result;
            }
            else
                if (v_Coil[(*Inst).Func_Index].Vector == CONTACT_VECTOR_Y)
                {
                    Y[index] = Result;
                }
                else
                    if (v_Coil[(*Inst).Func_Index].Vector == CONTACT_VECTOR_M)
                    {
                        M[index] = Result;
                    }
                    else
                        if (v_Coil[(*Inst).Func_Index].Vector == CONTACT_VECTOR_Z)
                        {
                            z[index] = Result;
                        }
        break;

        //Pulsed function
    case COIL_P:
        if (isFirstLoop)
        {
            if (v_Coil[(*Inst).Func_Index].Vector == CONTACT_VECTOR_Q)
            {
                Qaux[index] = Result;
            }
            else
                if (v_Coil[(*Inst).Func_Index].Vector == CONTACT_VECTOR_X)
                {
                    Xaux[index] = Result;
                }
                else
                    if (v_Coil[(*Inst).Func_Index].Vector == CONTACT_VECTOR_Y)
                    {
                        Yaux[index] = Result;
                    }
                    else
                        if (v_Coil[(*Inst).Func_Index].Vector == CONTACT_VECTOR_M)
                        {
                            Maux[index]=Result;
                        }
                        else
                            if (v_Coil[(*Inst).Func_Index].Vector == CONTACT_VECTOR_Z)
                            {
                                zaux[index]=Result;
                            }
        }
        else
        {
            if (v_Coil[(*Inst).Func_Index].Vector == CONTACT_VECTOR_Q && Qaux[index] == false && Result)
            {
                Q[index] = !Q[index];
                Qaux[index] = true;
                if (Q[index])

                    atualizasaidas(index, true);
                else
                    atualizasaidas(index, false);
            }
            else
                if (v_Coil[(*Inst).Func_Index].Vector == CONTACT_VECTOR_X && Xaux[index] == false && Result)
                {
                    X[index] = !X[index];
                    Xaux[index] = true;
                }
                else
                    if (v_Coil[(*Inst).Func_Index].Vector == CONTACT_VECTOR_Y && Yaux[index] == false && Result)
                    {
                        Y[index] = !Y[index];
                        Yaux[index] = true;
                    }
                    else
                        if (v_Coil[(*Inst).Func_Index].Vector == CONTACT_VECTOR_M && Maux[index] == false && Result)
                        {
                            M[index] = !M[index];
                            ProcessGetPowerState();
                            Maux[index] = true;
                        }
                        else
                            if (v_Coil[(*Inst).Func_Index].Vector == CONTACT_VECTOR_Q && Qaux[index] == true && !(Result))
                            {
                                Qaux[index] = false;
                            }
                            else
                                if (v_Coil[(*Inst).Func_Index].Vector == CONTACT_VECTOR_X && Xaux[index] == true && !(Result))
                                {
                                    Xaux[index] = false;
                                }
                                else
                                    if (v_Coil[(*Inst).Func_Index].Vector == CONTACT_VECTOR_Y && Yaux[index] == true && !(Result))
                                    {
                                        Yaux[index] = false;
                                    }
                                    else
                                        if (v_Coil[(*Inst).Func_Index].Vector == CONTACT_VECTOR_M && Maux[index] == true && !(Result))
                                        {
                                            Maux[index] = false;
                                        }
                                        else
                                            if (v_Coil[(*Inst).Func_Index].Vector == CONTACT_VECTOR_Z && zaux[index] == true && !(Result))
                                            {
                                                zaux[index] = false;
                                            }
        }
        break;
    //Set function
    case COIL_S:
        if (Result)
        {
            if (v_Coil[(*Inst).Func_Index].Vector == CONTACT_VECTOR_Q)
            {
                Q[index] = true;
                atualizasaidas(index, true);
                if (index == 0x01 && aux_ProductCounter)
                {
                    aux_ProductCounter = 0;
                }
            }
            else
                if (v_Coil[(*Inst).Func_Index].Vector == CONTACT_VECTOR_Y)
                {
                    Y[index] = true;
                }
                else
                    if (v_Coil[(*Inst).Func_Index].Vector == CONTACT_VECTOR_M)
                    {
                        M[index] = true;
                    }
                    else
                        if (v_Coil[(*Inst).Func_Index].Vector == CONTACT_VECTOR_X)
                        {
                            X[index] = true;
                        }
                        else
                            if (v_Coil[(*Inst).Func_Index].Vector == CONTACT_VECTOR_Z)
                            {
                                z[index] = true;
                            }
        }
        break;
    //Reset function
    case COIL_R:
        if (Result)
        {
            if (v_Coil[(*Inst).Func_Index].Vector == CONTACT_VECTOR_Q)
            {
                Q[index] = false;
                Qaux[index] = false;
                atualizasaidas(index, false);
                if (index == 0x01 && !aux_ProductCounter)
                {
                    ProductCounter++;
                    aux_ProductCounter = 1;
                    R[8] = ProductCounter>>8;
                    R[9] = ProductCounter&0x00FF;
                    Flash_rw(F_R+(unsigned int)(8), R[8]);
                    Flash_rw(F_R+(unsigned int)(9), R[9]);
                    ProcessGetCounter();
                }
            }
            else
                if (v_Coil[(*Inst).Func_Index].Vector == CONTACT_VECTOR_Y)
                {
                    Y[index] = false;
                    Yaux[index] = false;
                }
                else
                    if (v_Coil[(*Inst).Func_Index].Vector == CONTACT_VECTOR_M)
                    {
                        M[index] = false;
                        Maux[index] = false;
                        if (index == 0x0b)
                            ProcessGetPowerState();
                    }
                    else
                        if (v_Coil[(*Inst).Func_Index].Vector == CONTACT_VECTOR_X)
                        {
                            X[index] = false;
                            Xaux[index] = false;
                        }
                        else
                            if (v_Coil[(*Inst).Func_Index].Vector == CONTACT_VECTOR_Z)
                            {
                                z[index] = false;
                            }
        }
        break;

    }

    return i;
}


bool F_Timer(struct Instruction *Inst, unsigned int Index_Inst)
{

//        unsigned int Index;
    unsigned char Mode;
    unsigned char Base;
    unsigned long long int SetValue;
    unsigned long long int ReadValue;
    unsigned char Type;
    unsigned char State;
    unsigned char Contact_Reset;
    unsigned char Contact_Reset_Index;
    unsigned long long int Timer_Count;
    unsigned long long int Timer_Target;


    unsigned char index = v_Timer[(*Inst).Func_Index].Index;
//        if (index == tmp_index)
//            index == index;
    bool Result = GetInstructionResult(Inst);
    unsigned char i = 0;
    while (v_Instruction[Index_Inst+i].Has_Or_Instruction)
    {
        i++;
        Result |= GetInstructionResult(&v_Instruction[Index_Inst+i]);
    }


     switch(v_Timer[(*Inst).Func_Index].Mode)
     {

     case TIMER_MODE_T0:

         break;
     //Delay on Energization
     case TIMER_MODE_T1:
         if (Result)
         {
             v_Timer[index].Enabled = true;
             if (v_Timer[index].Timer_Target == 0)
             {
                 v_Timer[index].Enabled = true;
                 v_Timer[index].Timer_Target = Counter_ms + v_Timer[index].Timer_Count;
                 if (v_Timer[index].Timer_Target > 50000)
                     v_Timer[index].Timer_Target -= 50000;
                 v_Timer[index].ReadValue = 0;

             }
             else
                 if (v_Timer[index].Timer_Target > (v_Timer[index].Timer_Count > v_Timer[index].Timer_Target ? (Counter_ms > 10000 ? 0 : Counter_ms) : Counter_ms))
                 {


                     v_Timer[index].ReadValue++;
                 }
                 else
                 {
                     v_Timer[index].State = true;
                     T[index] = true;
                 }
         }
         else
         {

             v_Timer[index].Enabled = false;
             v_Timer[index].State = false;
             T[index] = false;
             v_Timer[index].Timer_Target = 0;
         }

         break;

     case TIMER_MODE_T2:

         break;

     case TIMER_MODE_T3:
         if (v_Timer[index].State == false && v_Timer[index].Timer_Target == 0 && Result)
         {
             if (index == 0x0D)
                 index == index;
             T[index] = true;
             v_Timer[index].State = true;
         }
         else
             if (v_Timer[index].State == true && v_Timer[index].Timer_Target == 0 && !(Result))
             {
                 v_Timer[index].Enabled = true;
                 v_Timer[index].Timer_Target = Counter_ms + v_Timer[index].Timer_Count;
                 if (v_Timer[index].Timer_Target > 50000)
                     v_Timer[index].Timer_Target -= 50000;
                 v_Timer[index].State = true;
                 T[index] = true;

             }
             else
                 if ((v_Timer[index].Timer_Target > (v_Timer[index].Timer_Count > v_Timer[index].Timer_Target ? (Counter_ms > 10000 ? 0 : Counter_ms) : Counter_ms)) && !(Result) && v_Timer[index].State == true)
                 {

                     v_Timer[index].ReadValue++;
                 }
                 else
                     if (v_Timer[index].Timer_Target <= Counter_ms && !(Result) && v_Timer[index].State == true)
                     {
                         T[index] = false;
                         v_Timer[index].State = false;
                         v_Timer[index].Timer_Target = 0;
                         v_Timer[index].ReadValue = 0;
                     }
         break;
     //Delay on De-energization
     case TIMER_MODE_T4:



         if (v_Timer[index].Enabled == false && v_Timer[index].Timer_Target == 0 && Result)
         {

             v_Timer[index].Enabled = true;
         }
         else
             if (v_Timer[index].Enabled == true && v_Timer[index].Timer_Target == 0 && !(Result))
             {
                 v_Timer[index].Enabled = false;
                 v_Timer[index].Timer_Target = Counter_ms + v_Timer[index].Timer_Count;
                 if (v_Timer[index].Timer_Target > 50000)
                     v_Timer[index].Timer_Target -= 50000;
                 v_Timer[index].State = true;
                 T[index] = true;

             }
             else
                 if ((v_Timer[index].Timer_Target > (v_Timer[index].Timer_Count > v_Timer[index].Timer_Target ? (Counter_ms > 10000 ? 0 : Counter_ms) : Counter_ms)) && !(Result))
                 {

                     v_Timer[index].ReadValue++;
                 }
                 else
                 {

                     v_Timer[index].State = false;
                     T[index] = false;
                     v_Timer[index].Timer_Target = 0;
                     v_Timer[index].ReadValue = 0;
                 }
         break;

     case TIMER_MODE_T5:

         break;

     case TIMER_MODE_T6:
         if (Result)
         {
             v_Timer[index].Enabled = true;
             if (v_Timer[index].Timer_Target == 0)
             {
                 v_Timer[index].Enabled = true;
                 v_Timer[index].Timer_Target = Counter_ms + v_Timer[index].Timer_Count;
                 if (v_Timer[index].Timer_Target > 50000)
                     v_Timer[index].Timer_Target -= 50000;
                 v_Timer[index].ReadValue = 0;

                 v_Timer[index].State = true;
                 T[index] = true;

             }
             else
                 if (v_Timer[index].Timer_Target > (v_Timer[index].Timer_Count > v_Timer[index].Timer_Target ? (Counter_ms > 10000 ? 0 : Counter_ms) : Counter_ms))
                 {
                     v_Timer[index].ReadValue++;
                 }
                 else
                 {
                     v_Timer[index].Timer_Target = Counter_ms + v_Timer[index].Timer_Count;
                     if (v_Timer[index].Timer_Target > 50000)
                         v_Timer[index].Timer_Target -= 50000;
                     v_Timer[index].ReadValue = 0;

                     v_Timer[index].State = (v_Timer[index].State ? false : true);
                     T[index] = v_Timer[index].State;
                 }
         }
         else
         {

             v_Timer[index].Enabled = false;
             v_Timer[index].State = false;
             T[index] = false;
             v_Timer[index].Timer_Target = 0;
         }
         break;

     }
     return i;
}

bool F_Stepper(struct Instruction *Inst, unsigned int Index_Inst)
{
    unsigned int index = v_Stepper[(*Inst).Func_Index].Index;
    if (index == tmp_index)
        index == index;
    bool Result = GetInstructionResult(Inst);
    unsigned int i = 0;
    while (v_Instruction[Index_Inst+i].Has_Or_Instruction)
    {
        i++;
        Result |= GetInstructionResult(&v_Instruction[Index_Inst+i]);
    }


    switch(v_Stepper[(*Inst).Func_Index].Stage)//(v_Stepper[index].Stage) ???
    {

    case STEPPER_OFF:
        if (Result)
        {
            v_Stepper[(*Inst).Func_Index].Stage = STEPPER_ACC;
            v_Stepper[(*Inst).Func_Index].ms_wait = 40;
            v_Stepper[(*Inst).Func_Index].aux_ms_wait = 0;
            v_Stepper[(*Inst).Func_Index].aux_inc = 0;
            v_Stepper[(*Inst).Func_Index].Timer_Acc_Target = v_Stepper[(*Inst).Func_Index].Timer_Acc_Count + Counter_ms;
            v_Stepper[(*Inst).Func_Index].Timer_Total_Target = v_Stepper[(*Inst).Func_Index].Timer_Total_Count + Counter_ms;
            v_Stepper[(*Inst).Func_Index].inc = v_Stepper[(*Inst).Func_Index].Timer_Acc_Count/(40-2400/v_Stepper[(*Inst).Func_Index].Speed);
        }
        break;
    case STEPPER_ACC:
        if (Result)
        {
            if (v_Stepper[(*Inst).Func_Index].Timer_Acc_Target > Counter_ms)
            {
                if (++(v_Stepper[(*Inst).Func_Index].aux_ms_wait) >= v_Stepper[(*Inst).Func_Index].ms_wait)
                {

                    Q[v_Stepper[(*Inst).Func_Index].Vector_Index] = (Q[v_Stepper[(*Inst).Func_Index].Vector_Index]== true ? false : true);
                    atualizasaidas(v_Stepper[(*Inst).Func_Index].Vector_Index, Q[v_Stepper[(*Inst).Func_Index].Vector_Index]);

                    if (++(v_Stepper[(*Inst).Func_Index].aux_inc) <= v_Stepper[(*Inst).Func_Index].inc)
                    {
                        v_Stepper[(*Inst).Func_Index].ms_wait -= (v_Stepper[(*Inst).Func_Index].ms_wait >= 4 ? 1 : 0);
                        v_Stepper[(*Inst).Func_Index].aux_inc = 0;
                    }
                    v_Stepper[(*Inst).Func_Index].aux_ms_wait = 0;
                }
            }
            else
            {
                v_Stepper[(*Inst).Func_Index].Stage = STEPPER_CONST;
                v_Stepper[(*Inst).Func_Index].ms_wait = 2400/v_Stepper[(*Inst).Func_Index].Speed;
            }
        }
        else
        {
            v_Stepper[(*Inst).Func_Index].Stage = STEPPER_OFF;
        }
        break;
    case STEPPER_CONST:
        if (Result)
        {
            if (v_Stepper[(*Inst).Func_Index].Timer_Total_Target > Counter_ms)
            {
                if (++(v_Stepper[(*Inst).Func_Index].aux_ms_wait) >= v_Stepper[(*Inst).Func_Index].ms_wait)
                {
                    Q[v_Stepper[(*Inst).Func_Index].Vector_Index] = (Q[v_Stepper[(*Inst).Func_Index].Vector_Index]== true ? false : true);
                    atualizasaidas(v_Stepper[(*Inst).Func_Index].Vector_Index, Q[v_Stepper[(*Inst).Func_Index].Vector_Index]);

                    v_Stepper[(*Inst).Func_Index].aux_ms_wait = 0;
                }
            }
        }
        else
        {
            v_Stepper[(*Inst).Func_Index].Stage = STEPPER_DEC;
        }
        break;
    case STEPPER_DEC:
        Q[v_Stepper[(*Inst).Func_Index].Vector_Index] = 0;
        atualizasaidas(v_Stepper[(*Inst).Func_Index].Vector_Index, false);
        v_Stepper[(*Inst).Func_Index].Stage = STEPPER_OFF;
        break;
    default:
        break;

    }
    return i;
}

void SetContact(unsigned int Index, unsigned char Vector, unsigned char VectorIndex, unsigned char isNO)
{
    v_Contact[Index].Vector = Vector;//CONTACT_VECTOR_I
    v_Contact[Index].Index = VectorIndex;
    v_Contact[Index].isNO = isNO;
    v_Contact[Index].State = false;
}

void SetCoil(unsigned int Index, unsigned char Vector, unsigned char Type, unsigned char VectorIndex)
{
    v_Coil[Index].Vector = Vector;//CONTACT_VECTOR_M
    v_Coil[Index].Type = Type;//COIL_P
    v_Coil[Index].Index = VectorIndex;
}

void SetTimer(unsigned int Index, unsigned char Mode, unsigned char Base, unsigned int SetValue, unsigned char Type, unsigned char ResetVector, unsigned char ResetIndex)
{
    unsigned int aux_value = SetValue;
    SetValue = (Index == 1 ? (R[16]<<8)|R[17] : (Index == 2 ? (R[18]<<8)|R[19] : (Index == 3 ? (R[20]<<8)|R[21] : SetValue)));
    SetValue = (Index == 4 ? (R[22]<<8)|R[23] : (Index == 5 ? (R[24]<<8)|R[25] : (Index == 6 ? (R[26]<<8)|R[27] : SetValue)));
    if (SetValue > 3000)
        SetValue = aux_value;

    v_Timer[Index].Index = Index;
    v_Timer[Index].Mode = Mode;//TIMER_MODE_T4
    v_Timer[Index].Base = Base;//TIMER_BASE::MS1
    v_Timer[Index].SetValue = SetValue;
    v_Timer[Index].ReadValue = 0;
    v_Timer[Index].Type = Type;
    v_Timer[Index].State = false;
    v_Timer[Index].Contact_Reset = ResetVector;//CONTACT_VECTOR_ZERO
    v_Timer[Index].Contact_Reset_Index = ResetIndex;
    v_Timer[Index].Timer_Count = (Base == TIMER_BASE_MS1 ? SetValue : Base == TIMER_BASE_MS10 ? 10*SetValue : Base == TIMER_BASE_MS100 ? 100*SetValue : Base == TIMER_BASE_SEC ? 1000*SetValue : Base == TIMER_BASE_MIN ? 600*SetValue : 0);
    v_Timer[Index].Timer_Count /= 5;
    v_Timer[Index].Timer_Target = 0;
    v_Timer[Index].Enabled = false;
}

void SetStepper(unsigned int Index, unsigned char Mode, unsigned char Base,
                    unsigned int Speed, unsigned int SetAccValue, unsigned int SetTotalValue,
                    unsigned int SetDecValue, unsigned char Vector, unsigned char Vector_Index)
{
    unsigned int aux_speed = Speed, aux_Value = SetTotalValue;
    Speed = (Index == 1 ? (R[10]<<8)|R[11] : (Index == 2 ? (R[12]<<8)|R[13] : (Index == 3 ? (R[14]<<8)|R[15] : Speed)));
    SetTotalValue = (Index == 1 ? (R[22]<<8)|R[23] : (Index == 2 ? (R[24]<<8)|R[25] : (Index == 3 ? (R[26]<<8)|R[27] : SetTotalValue)));
    if (Speed > 2400)
        Speed = aux_speed;
    if (SetTotalValue > 3000)
        SetTotalValue = aux_value;
    v_Stepper[Index].Index = Index;
    v_Stepper[Index].Mode = Mode;//TIMER_MODE_T4
    v_Stepper[Index].Base = Base;//TIMER_BASE::MS1
    v_Stepper[Index].Speed = Speed;
    v_Stepper[Index].ms_wait = 2400/Speed;
    v_Stepper[Index].SetAccValue = SetAccValue;
    v_Stepper[Index].SetTotalValue = SetTotalValue;
    v_Stepper[Index].SetDecValue = SetDecValue;
    v_Stepper[Index].ReadValue = 0;
    v_Stepper[Index].Stage = STEPPER_OFF;
    v_Stepper[Index].State = false;
    v_Stepper[Index].Timer_Acc_Count = (Base == TIMER_BASE_MS1 ? SetAccValue : Base == TIMER_BASE_MS10 ? 10*SetAccValue : Base == TIMER_BASE_MS100 ? 100*SetAccValue : Base == TIMER_BASE_SEC ? 1000*SetAccValue : Base == TIMER_BASE_MIN ? 600*SetAccValue : 0);
    v_Stepper[Index].Timer_Acc_Count /= 5;
    v_Stepper[Index].Timer_Acc_Target = 0;
    v_Stepper[Index].Timer_Total_Count = (Base == TIMER_BASE_MS1 ? SetTotalValue : Base == TIMER_BASE_MS10 ? 10*SetTotalValue : Base == TIMER_BASE_MS100 ? 100*SetTotalValue : Base == TIMER_BASE_SEC ? 1000*SetTotalValue : Base == TIMER_BASE_MIN ? 600*SetTotalValue : 0);
    v_Stepper[Index].Timer_Total_Count /= 5;
    v_Stepper[Index].Timer_Total_Target = 0;
    v_Stepper[Index].Timer_Dec_Count = (Base == TIMER_BASE_MS1 ? SetDecValue : Base == TIMER_BASE_MS10 ? 10*SetDecValue : Base == TIMER_BASE_MS100 ? 100*SetDecValue : Base == TIMER_BASE_SEC ? 1000*SetDecValue : Base == TIMER_BASE_MIN ? 600*SetDecValue : 0);
    v_Stepper[Index].Timer_Dec_Count /= 5;
    v_Stepper[Index].Timer_Dec_Target = 0;
    v_Stepper[Index].Vector = Vector;
    v_Stepper[Index].Vector_Index = Vector_Index;

}

void SetInstruction(unsigned int Index, unsigned char Func_Type, unsigned int Func_Index, bool HasOrInstruction, struct Contact Contact_1, struct Contact Contact_2, struct Contact Contact_3)
{
    v_Instruction[Index].Func_Type = Func_Type;
    v_Instruction[Index].Func_Index = Func_Index;
    v_Instruction[Index].Inst_Contact_1 = Contact_1;
    v_Instruction[Index].Inst_Contact_2 = Contact_2;
    v_Instruction[Index].Inst_Contact_3 = Contact_3;
    v_Instruction[Index].Has_Or_Instruction = HasOrInstruction;

}

void SetNewCoilInstruction(unsigned char Vector_1, unsigned char VectorIndex_1, unsigned char isNO_1,
                           unsigned char Vector_2, unsigned char VectorIndex_2, unsigned char isNO_2,
                           unsigned char Vector_3, unsigned char VectorIndex_3, unsigned char isNO_3,
                           unsigned char Vector_Coil, unsigned char VectorIndex_Coil, unsigned char Type_Coil,
                           bool HasOrInstruction)
{
    SetContact(aux_Contact++, Vector_1, VectorIndex_1, isNO_1);
    SetContact(aux_Contact++, Vector_2, VectorIndex_2, isNO_2);
    SetContact(aux_Contact++, Vector_3, VectorIndex_3, isNO_3);
    if (!(aux_Inst == 0 ? 0 : v_Instruction[aux_Inst-1].Has_Or_Instruction))
        SetCoil(aux_Coil++, Vector_Coil, Type_Coil, VectorIndex_Coil);

    SetInstruction(aux_Inst++, FUNC_COIL, aux_Coil-1, HasOrInstruction, v_Contact[aux_Contact-3], v_Contact[aux_Contact-2], v_Contact[aux_Contact-1]);
}

void SetNewTimerInstruction(unsigned char Vector_1, unsigned char VectorIndex_1, unsigned char isNO_1,
                                   unsigned char Vector_2, unsigned char VectorIndex_2, unsigned char isNO_2,
                                   unsigned char Vector_3, unsigned char VectorIndex_3, unsigned char isNO_3,
                                   unsigned char Timer_Index, unsigned char Mode, unsigned char Base,
                                   unsigned int SetValue, unsigned char Type, unsigned char ResetVector,
                                   unsigned char ResetIndex, bool HasOrInstruction)
{
    SetContact(aux_Contact++, Vector_1, VectorIndex_1, isNO_1);
    SetContact(aux_Contact++, Vector_2, VectorIndex_2, isNO_2);
    SetContact(aux_Contact++, Vector_3, VectorIndex_3, isNO_3);
    if (!(aux_Inst == 0 ? 0 : v_Instruction[aux_Inst-1].Has_Or_Instruction))
        SetTimer(Timer_Index, Mode, Base, SetValue, Type, ResetVector, ResetIndex);

    SetInstruction(aux_Inst++, FUNC_TIMER, Timer_Index, HasOrInstruction, v_Contact[aux_Contact-3], v_Contact[aux_Contact-2], v_Contact[aux_Contact-1]);

}

void SetNewStepperInstruction(unsigned char Vector_1, unsigned char VectorIndex_1, unsigned char isNO_1,
                                  unsigned char Vector_2, unsigned char VectorIndex_2, unsigned char isNO_2,
                                  unsigned char Vector_3, unsigned char VectorIndex_3, unsigned char isNO_3,
                                  unsigned char Stepper_Index, unsigned char Base, unsigned int Speed,
                                  unsigned int SetAccValue, unsigned int SetTotalValue, unsigned int SetDecValue,
                                  unsigned char Vector, unsigned char Vector_Index, bool HasOrInstruction)
{
    SetContact(aux_Contact++, Vector_1, VectorIndex_1, isNO_1);
    SetContact(aux_Contact++, Vector_2, VectorIndex_2, isNO_2);
    SetContact(aux_Contact++, Vector_3, VectorIndex_3, isNO_3);
    if (!(aux_Inst == 0 ? 0 : v_Instruction[aux_Inst-1].Has_Or_Instruction))
        SetStepper(Stepper_Index, 0, Base, Speed, SetAccValue, SetTotalValue, SetDecValue, Vector, Vector_Index);

    SetInstruction(aux_Inst++, FUNC_STEPPER, Stepper_Index, HasOrInstruction, v_Contact[aux_Contact-3], v_Contact[aux_Contact-2], v_Contact[aux_Contact-1]);
}


void Fail_LoopCycleTimeout() //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
{
    POUT(PC, ERRO, 0x00); //Gerar erro: piscar led de erro para informar o timeout               <<<<<<<<<<<ACENDER INDICANDO ESTOURO DE TIMMEOUT
}

void ProcessFrame()
{
    switch (FrameRx[FRAME_CMD_2])
    {
    case MODBUS_WRITE_SINGLE_COIL:
        break;
    case MODBUS_WRITE_SINGLE_REGISTER:
        ProcessWriteSingleRegister();
        break;
    case MODBUS_READ_SPEED_REGISTER:
        ProcessGetSpeed();
        break;
    case MODBUS_READ_LENGTH_REGISTER:
        ProcessGetLength();
        break;
    case MODBUS_WRITE_SINGLE_REGISTER_SIZE:
        break;
    case MODBUS_REQUEST_CONFIG:
        aux_GetConfigs = 1;

    }

    for (contFrame = 0; contFrame < 20; contFrame++)
        FrameRx[contFrame] = 0;
    isFrameReady = false;
}

void ProcessGetConfigs()
{
    switch (aux_GetConfigs++)
    {
    case 1:
        ProcessGetSpeed();
        break;
    case 2:
        ProcessGetLength();
        break;
    case 3:
        ProcessGetCounter();
        break;
    case 4:
        ProcessGetPowerState();
        break;
    case 5:
        ProcessGetSensorState();
        break;
    case 6:
        FrameTx[9] = 0x22;
        FrameTx[10] = 0x01;
        FrameTx[11] = R[10];
        FrameTx[12] = R[11];
        ProcessReturn();
        break;
    case 7:
        FrameTx[9] = 0x22;
        FrameTx[10] = 0x02;
        FrameTx[11] = R[12];
        FrameTx[12] = R[13];
        ProcessReturn();
        break;
    case 8:
        FrameTx[9] = 0x22;
        FrameTx[10] = 0x03;
        FrameTx[11] = R[14];
        FrameTx[12] = R[15];
        ProcessReturn();
        break;
    case 9:
        FrameTx[9] = 0x22;
        FrameTx[10] = 0x04;
        FrameTx[11] = R[16];
        FrameTx[12] = R[17];
        ProcessReturn();
        break;
    case 10:
        FrameTx[9] = 0x22;
        FrameTx[10] = 0x05;
        FrameTx[11] = R[18];
        FrameTx[12] = R[19];
        ProcessReturn();
        break;
    case 11:
        FrameTx[9] = 0x22;
        FrameTx[10] = 0x06;
        FrameTx[11] = R[20];
        FrameTx[12] = R[21];
        ProcessReturn();
        break;
    case 12:
        FrameTx[9] = 0x22;
        FrameTx[10] = 0x07;
        FrameTx[11] = R[22];
        FrameTx[12] = R[23];
        ProcessReturn();
        break;
    case 13:
        FrameTx[9] = 0x22;
        FrameTx[10] = 0x08;
        FrameTx[11] = R[24];
        FrameTx[12] = R[25];
        ProcessReturn();
        break;
    case 14:
        FrameTx[9] = 0x22;
        FrameTx[10] = 0x09;
        FrameTx[11] = R[26];
        FrameTx[12] = R[27];
        ProcessReturn();
        break;
    case 15:

        aux_GetConfigs = 0;
        break;
    }
}

void ProcessGetSpeed()
{
    FrameTx[0] = 0x7e;
    FrameTx[1] = 0x00;
    FrameTx[2] = 0x00;
    FrameTx[3] = 0x00;
    FrameTx[4] = 0x00;
    FrameTx[5] = 0x0E;
    FrameTx[6] = 0x06;
    FrameTx[7] = 0x00;
    FrameTx[8] = MODBUS_WRITE_SINGLE_REGISTER;
    FrameTx[9] = 0x13;
    FrameTx[10] = 0x00;
    FrameTx[11] = R[4];
    FrameTx[12] = R[5];
    FrameTx[13] = checksum(FrameTx, 14);

    sendTx(14);
}


void ProcessGetLength()
{
    FrameTx[0] = 0x7e;
    FrameTx[1] = 0x00;
    FrameTx[2] = 0x00;
    FrameTx[3] = 0x00;
    FrameTx[4] = 0x00;
    FrameTx[5] = 0x0E;
    FrameTx[6] = 0x06;
    FrameTx[7] = 0x00;
    FrameTx[8] = MODBUS_WRITE_SINGLE_REGISTER;
    FrameTx[9] = 0x13;
    FrameTx[10] = 0x01;
    FrameTx[11] = R[6];
    FrameTx[12] = R[7];
    FrameTx[13] = checksum(FrameTx, 14);

    sendTx(14);
}

void ProcessReturn()
{
    FrameTx[0] = 0x7e;
    FrameTx[1] = 0x00;
    FrameTx[2] = 0x00;
    FrameTx[3] = 0x00;
    FrameTx[4] = 0x00;
    FrameTx[5] = 0x0E;
    FrameTx[6] = 0x06;
    FrameTx[7] = 0x00;
    FrameTx[8] = MODBUS_WRITE_SINGLE_REGISTER;
//    FrameTx[9] = 0x13;
//    FrameTx[10] = 0x01;
//    FrameTx[11] = R[6];
//    FrameTx[12] = R[7];
    FrameTx[13] = checksum(FrameTx, 14);

    sendTx(14);
}

void ProcessGetCounter()
{
    FrameTx[0] = 0x7e;
    FrameTx[1] = 0x00;
    FrameTx[2] = 0x00;
    FrameTx[3] = 0x00;
    FrameTx[4] = 0x00;
    FrameTx[5] = 0x0E;
    FrameTx[6] = 0x06;
    FrameTx[7] = 0x00;
    FrameTx[8] = MODBUS_WRITE_SINGLE_REGISTER;
    FrameTx[9] = 0x12;
    FrameTx[10] = 0x00;

//    FrameTx[11] = diff_ef[indice_diff_ef-1]>>8;
//    FrameTx[12] = diff_ef[indice_diff_ef-1]&0x00FF;
    FrameTx[11] = R[8];
    FrameTx[12] = R[9];
    FrameTx[13] = checksum(FrameTx, 14);

    sendTx(14);
}

void ProcessGetPowerState()
{
    FrameTx[0] = 0x7e;
    FrameTx[1] = 0x00;
    FrameTx[2] = 0x00;
    FrameTx[3] = 0x00;
    FrameTx[4] = 0x00;
    FrameTx[5] = 0x0E;
    FrameTx[6] = 0x06;
    FrameTx[7] = 0x00;
    FrameTx[8] = MODBUS_WRITE_SINGLE_REGISTER;
    FrameTx[9] = 0x10;
    FrameTx[10] = 0x03;
    FrameTx[11] = 0x00;
    FrameTx[12] = (X[0x10] ? 100 : 101);
    FrameTx[13] = checksum(FrameTx, 14);

    sendTx(14);
}
void ProcessGetSensorState()
{
        FrameTx[0] = 0x7e;
        FrameTx[1] = 0x00;
        FrameTx[2] = 0x00;
        FrameTx[3] = 0x00;
        FrameTx[4] = 0x00;
        FrameTx[5] = 0x0E;
        FrameTx[6] = 0x06;
        FrameTx[7] = 0x00;
        FrameTx[8] = MODBUS_WRITE_SINGLE_REGISTER;
        FrameTx[9] = 0x10;
        FrameTx[10] = 0x04;
        FrameTx[11] = 0x00;
        FrameTx[12] = (Y[0x12] ? 100 : 101);
        FrameTx[13] = checksum(FrameTx, 14);

        sendTx(14);
}
void ProcessGetTemperatureState()
{
        FrameTx[0] = 0x7e;
        FrameTx[1] = 0x00;
        FrameTx[2] = 0x00;
        FrameTx[3] = 0x00;
        FrameTx[4] = 0x00;
        FrameTx[5] = 0x0E;
        FrameTx[6] = 0x06;
        FrameTx[7] = 0x00;
        FrameTx[8] = MODBUS_WRITE_SINGLE_REGISTER;
        FrameTx[9] = 0x10;
        FrameTx[10] = 0x07;
        FrameTx[11] = 0x00;
        FrameTx[12] = (Y[0x0C] ? 100 : 101);
        FrameTx[13] = checksum(FrameTx, 14);

        sendTx(14);
}
void ProcessGetStampState()
{
        FrameTx[0] = 0x7e;
        FrameTx[1] = 0x00;
        FrameTx[2] = 0x00;
        FrameTx[3] = 0x00;
        FrameTx[4] = 0x00;
        FrameTx[5] = 0x0E;
        FrameTx[6] = 0x06;
        FrameTx[7] = 0x00;
        FrameTx[8] = MODBUS_WRITE_SINGLE_REGISTER;
        FrameTx[9] = 0x10;
        FrameTx[10] = 0x08;
        FrameTx[11] = 0x00;
        FrameTx[12] = (Y[0x0D] ? 100 : 101);
        FrameTx[13] = checksum(FrameTx, 14);

        sendTx(14);
}


void ProcessWriteSingleRegister()
{
    aux_value = FrameRx[FRAME_CMD_2+REQ_WRITE_SINGLE_REGISTER_AL];
    switch (FrameRx[FRAME_CMD_2+REQ_WRITE_SINGLE_REGISTER_AH]&0xF0)
    {
    case VECTOR_I:
        Flash_rw(F_I+(unsigned int)((aux_value&0x18)>>3), (FrameRx[FRAME_CMD_2+REQ_WRITE_SINGLE_REGISTER_VL] ?
                         *(F_I+(unsigned int)((aux_value&0x18)>>3))|(0x01<<(aux_value&0x07)) :
                         *(F_I+(unsigned int)((aux_value&0x18)>>3))&~(0x01<<(aux_value&0x07))));
        I[aux_value] = FrameRx[FRAME_CMD_2+REQ_WRITE_SINGLE_REGISTER_VL];
        break;
    case VECTOR_X:
        Flash_rw(F_X+(unsigned int)((aux_value&0x18)>>3), (FrameRx[FRAME_CMD_2+REQ_WRITE_SINGLE_REGISTER_VL] ?
                         *(F_X+(unsigned int)((aux_value&0x18)>>3))|(0x01<<(aux_value&0x07)) :
                         *(F_X+(unsigned int)((aux_value&0x18)>>3))&~(0x01<<(aux_value&0x07))));
        X[aux_value] = FrameRx[FRAME_CMD_2+REQ_WRITE_SINGLE_REGISTER_VL];
        if (aux_value == 0x10)
        {
            modificar_estado = 1;
            aux_cont_ligar = 0;
            ProcessGetPowerState();
//            FrameTx[0] = 0x7e;
//            FrameTx[1] = 0x00;
//            FrameTx[2] = 0x00;
//            FrameTx[3] = 0x00;
//            FrameTx[4] = 0x00;
//            FrameTx[5] = 0x0E;
//            FrameTx[6] = 0x06;
//            FrameTx[7] = 0x00;
//            FrameTx[8] = MODBUS_WRITE_SINGLE_REGISTER;
//            FrameTx[9] = 0x10;
//            FrameTx[10] = 0x03;
//            FrameTx[11] = 0x00;
//            FrameTx[12] = (X[aux_value] ? 101 : 100);
//            FrameTx[13] = checksum(FrameTx, 14);
//
//            sendTx(14);
        }
        break;
    case VECTOR_Y:
        Flash_rw(F_Y+(unsigned int)((aux_value&0x18)>>3), (FrameRx[FRAME_CMD_2+REQ_WRITE_SINGLE_REGISTER_VL] ?
                         *(F_Y+(unsigned int)((aux_value&0x18)>>3))|(0x01<<(aux_value&0x07)) :
                         *(F_Y+(unsigned int)((aux_value&0x18)>>3))&~(0x01<<(aux_value&0x07))));
        Y[aux_value] = FrameRx[FRAME_CMD_2+REQ_WRITE_SINGLE_REGISTER_VL];
        if (aux_value == 0x10)
        {
            FrameTx[0] = 0x7e;
            FrameTx[1] = 0x00;
            FrameTx[2] = 0x00;
            FrameTx[3] = 0x00;
            FrameTx[4] = 0x00;
            FrameTx[5] = 0x0E;
            FrameTx[6] = 0x06;
            FrameTx[7] = 0x00;
            FrameTx[8] = MODBUS_WRITE_SINGLE_REGISTER;
            FrameTx[9] = 0x10;
            FrameTx[10] = 0x04;
            FrameTx[11] = 0x00;
            FrameTx[12] = (Y[aux_value] ? 100 : 101);
            FrameTx[13] = checksum(FrameTx, 14);

            sendTx(14);
        }

        if (aux_value == 0x12)
        {
            FrameTx[0] = 0x7e;
            FrameTx[1] = 0x00;
            FrameTx[2] = 0x00;
            FrameTx[3] = 0x00;
            FrameTx[4] = 0x00;
            FrameTx[5] = 0x0E;
            FrameTx[6] = 0x06;
            FrameTx[7] = 0x00;
            FrameTx[8] = MODBUS_WRITE_SINGLE_REGISTER;
            FrameTx[9] = 0x10;
            FrameTx[10] = 0x06;
            FrameTx[11] = 0x00;
            FrameTx[12] = (Y[aux_value] ? 100 : 101);
            FrameTx[13] = checksum(FrameTx, 14);

            sendTx(14);
        }
        break;
    case VECTOR_Z:
        Flash_rw(F_z+(unsigned int)((aux_value&0x18)>>3), (FrameRx[FRAME_CMD_2+REQ_WRITE_SINGLE_REGISTER_VL] ?
                         *(F_z+(unsigned int)((aux_value&0x18)>>3))|(0x01<<(aux_value&0x07)) :
                         *(F_z+(unsigned int)((aux_value&0x18)>>3))&~(0x01<<(aux_value&0x07))));
        z[aux_value] = FrameRx[FRAME_CMD_2+REQ_WRITE_SINGLE_REGISTER_VL];
        break;
    case VECTOR_Q:
        Flash_rw(F_Q+(unsigned int)((aux_value&0x18)>>3), (FrameRx[FRAME_CMD_2+REQ_WRITE_SINGLE_REGISTER_VL] ?
                         *(F_Q+(unsigned int)((aux_value&0x18)>>3))|(0x01<<(aux_value&0x07)) :
                         *(F_Q+(unsigned int)((aux_value&0x18)>>3))&~(0x01<<(aux_value&0x07))));
        Q[aux_value] = FrameRx[FRAME_CMD_2+REQ_WRITE_SINGLE_REGISTER_VL];
        atualizasaidas(aux_value, FrameRx[FRAME_CMD_2+REQ_WRITE_SINGLE_REGISTER_VL]);
        break;
    case VECTOR_M:
        Flash_rw(F_M+(unsigned int)((aux_value&0x18)>>3), (FrameRx[FRAME_CMD_2+REQ_WRITE_SINGLE_REGISTER_VL] ?
                         *(F_M+(unsigned int)((aux_value&0x18)>>3))|(0x01<<(aux_value&0x07)) :
                         *(F_M+(unsigned int)((aux_value&0x18)>>3))&~(0x01<<(aux_value&0x07))));
        M[aux_value] = FrameRx[FRAME_CMD_2+REQ_WRITE_SINGLE_REGISTER_VL];
        break;
    case VECTOR_T:
        Flash_rw(F_T+(unsigned int)((aux_value&0x18)>>3), (FrameRx[FRAME_CMD_2+REQ_WRITE_SINGLE_REGISTER_VL] ?
                         *(F_T+(unsigned int)((aux_value&0x18)>>3))|(0x01<<(aux_value&0x07)) :
                         *(F_T+(unsigned int)((aux_value&0x18)>>3))&~(0x01<<(aux_value&0x07))));
        T[aux_value] = FrameRx[FRAME_CMD_2+REQ_WRITE_SINGLE_REGISTER_VL];
        break;
    case VECTOR_R:
        if (aux_value == 0x00)
        {
            X[0x12] = 1;
            aux_value2 = (R[4]<<8)|(R[5]);
            aux_value2++;
            if (aux_value2 > 120)
                aux_value2 = 120;
            if (aux_value2 < 20)
                aux_value2 = 20;
            R[4] = aux_value2>>8;
            R[5] = aux_value2&0x00ff;
            ProcessGetSpeed();

            ppm =  (R[4]<<8)|R[5];
            timeout = 0;
            velocidade_esteira = ppm*35;
            velocidade_filme = (tamanho+6)*ppm*3/17;
        }
        if (aux_value == 0x01)
        {
            X[0x13] = 1;
            aux_value2 = ((R[4]<<8)|R[5])-1;
            if (aux_value2 > 120)
                aux_value2 = 120;
            if (aux_value2 < 20)
                aux_value2 = 20;
            R[4] = aux_value2>>8;
            R[5] = aux_value2&0x00ff;
            ProcessGetSpeed();


            ppm =  (R[4]<<8)|R[5];
            velocidade_esteira = ppm*35;
            velocidade_filme = (tamanho+6)*ppm*3/17;
        }
        if (aux_value == 0x02)
        {
            X[0x13] = 1;
//            aux_value2 = (FrameRx[11]<<8)&FrameRx[12];
            R[4] = FrameRx[11];
            R[5] = FrameRx[12];

            ppm =  (R[4]<<8)|R[5];

            if (ppm > 120)
                ppm = 120;
            if (ppm < 20)
                ppm = 20;
            R[4] = ppm>>8;
            R[5] = ppm&0x00ff;
            velocidade_esteira = ppm*35;
            velocidade_filme = (tamanho+6)*ppm*3/17;
            ProcessGetSpeed();
        }
        if (aux_value == 0x03)
        {
            aux_value2 = ((R[6]<<8)|R[7])+1;
            if (aux_value2 > 250)
                aux_value2 = 250;
            if (aux_value2 < 150)
                aux_value2 = 150;

            R[6] = aux_value2>>8;
            R[7] = aux_value2&0x00ff;


            tamanho =  (R[6]<<8)|R[7];
            velocidade_filme = (tamanho+6)*ppm*3/17;


            ProcessGetLength();
        }
        if (aux_value == 0x04)
        {
            aux_value2 = ((R[6]<<8)|R[7])-1;
            if (aux_value2 > 250)
                aux_value2 = 250;
            if (aux_value2 < 150)
                aux_value2 = 150;
            R[6] = aux_value2>>8;
            R[7] = aux_value2&0x00ff;
            tamanho =  (R[6]<<8)|R[7];
            velocidade_filme = (tamanho+6)*ppm*3/17;

            ProcessGetLength();
        }
        if (aux_value == 0x05)
        {
            R[6] = FrameRx[11];
            R[7] = FrameRx[12];
            tamanho =  (R[6]<<8)|R[7];
            if (tamanho > 250)
                tamanho = 250;
            if (tamanho < 150)
                tamanho = 150;
            R[6] = tamanho>>8;
            R[7] = tamanho&0x00ff;
            velocidade_filme = (tamanho+6)*ppm*3/17;

            ProcessGetLength();
        }
        if (aux_value == 0x08)
        {
            R[8]=0;
            R[9]=0;
            ProductCounter = 0;

            Flash_rw(F_R+(unsigned int)(8), R[8]);
            Flash_rw(F_R+(unsigned int)(9), R[9]);
            ProcessGetCounter();
        }
        if (aux_value == 0x00 || aux_value == 0x01)
        {
            /*copy_X2B(SEG_D, F_R+(unsigned int)(4),F_R+(unsigned int)(5),
                     R[4], R[5]);*/
//            Flash_rw(F_R+(unsigned int)(4), R[4]);
//            Flash_rw(F_R+(unsigned int)(5), R[5]);
        }
        if (aux_value == 0x03 || aux_value == 0x04)
        {
            /*copy_X2B(SEG_D, F_R+(unsigned int)(6),F_R+(unsigned int)(7),
                     R[6], R[7]);*/
//            Flash_rw(F_R+(unsigned int)(6), R[6]);
//            Flash_rw(F_R+(unsigned int)(7), R[7]);
        }

        if (aux_value == 0x0a)//Velocidade Lacre
        {
            R[10] = FrameRx[11];
            R[11] = FrameRx[12];
            tmp_rx = (R[10]<<8)|R[11];
            if (tmp_rx > 2400)
                tmp_rx = 2400;
            if (tmp_rx < 10)
                tmp_rx = 10;
            v_Stepper[1].Speed = tmp_rx;
            v_Stepper[1].ms_wait = 2400/tmp_rx;

            R[10] = tmp_rx>>8;
            R[11] = tmp_rx&0x00ff;
            Flash_rw(F_R+(unsigned int)(10), R[10]);
            Flash_rw(F_R+(unsigned int)(11), R[11]);

            FrameTx[9] = 0x22;
            FrameTx[10] = 0x01;
            FrameTx[11] = R[10];
            FrameTx[12] = R[11];
            ProcessReturn();
        }
        if (aux_value == 0x0b)//Velocidade Selo
        {
            R[12] = FrameRx[11];
            R[13] = FrameRx[12];
            tmp_rx = (R[12]<<8)|R[13];
            if (tmp_rx > 2400)
                tmp_rx = 2400;
            if (tmp_rx < 10)
                tmp_rx = 10;
            v_Stepper[2].Speed = tmp_rx;
            v_Stepper[2].ms_wait = 2400/tmp_rx;

            R[12] = tmp_rx>>8;
            R[13] = tmp_rx&0x00ff;
            Flash_rw(F_R+(unsigned int)(12), R[12]);
            Flash_rw(F_R+(unsigned int)(13), R[13]);

            FrameTx[9] = 0x22;
            FrameTx[10] = 0x02;
            FrameTx[11] = R[12];
            FrameTx[12] = R[13];
            ProcessReturn();
        }
        if (aux_value == 0x0c)//Velocidade Faca
        {
            R[14] = FrameRx[11];
            R[15] = FrameRx[12];
            tmp_rx = (R[14]<<8)|R[15];
            if (tmp_rx > 2400)
                tmp_rx = 2400;
            if (tmp_rx < 10)
                tmp_rx = 10;

            v_Stepper[3].Speed = tmp_rx;
            v_Stepper[3].ms_wait = 2400/tmp_rx;

            R[14] = tmp_rx>>8;
            R[15] = tmp_rx&0x00ff;
            Flash_rw(F_R+(unsigned int)(14), R[14]);
            Flash_rw(F_R+(unsigned int)(15), R[15]);

            FrameTx[9] = 0x22;
            FrameTx[10] = 0x03;
            FrameTx[11] = R[14];
            FrameTx[12] = R[15];
            ProcessReturn();
        }
        if (aux_value == 0x0d)//Atraso Lacre
        {
            R[16] = FrameRx[11];
            R[17] = FrameRx[12];
            tmp_rx = (R[16]<<8)|R[17];
            if (tmp_rx > 200)
                tmp_rx = 200;
            if (tmp_rx < 10)
                tmp_rx = 10;
            v_Timer[1].SetValue = tmp_rx;
            v_Timer[1].Timer_Count = tmp_rx;
            v_Timer[1].Timer_Count /= 5;

            R[16] = tmp_rx>>8;
            R[17] = tmp_rx&0x00ff;
            Flash_rw(F_R+(unsigned int)(16), R[16]);
            Flash_rw(F_R+(unsigned int)(17), R[17]);

            FrameTx[9] = 0x22;
            FrameTx[10] = 0x04;
            FrameTx[11] = R[16];
            FrameTx[12] = R[17];
            ProcessReturn();
        }
        if (aux_value == 0x0e)//Atraso Selo
        {
            R[18] = FrameRx[11];
            R[19] = FrameRx[12];
            tmp_rx = (R[18]<<8)|R[19];
            if (tmp_rx > 200)
                tmp_rx = 200;
            if (tmp_rx < 10)
                tmp_rx = 10;
            v_Timer[2].SetValue = tmp_rx;
            v_Timer[2].Timer_Count = tmp_rx;
            v_Timer[2].Timer_Count /= 5;

            R[18] = tmp_rx>>8;
            R[19] = tmp_rx&0x00ff;
            Flash_rw(F_R+(unsigned int)(18), R[18]);
            Flash_rw(F_R+(unsigned int)(19), R[19]);

            FrameTx[9] = 0x22;
            FrameTx[10] = 0x05;
            FrameTx[11] = R[18];
            FrameTx[12] = R[19];
            ProcessReturn();
        }
        if (aux_value == 0x0f)//Atraso Faca
        {
            R[20] = FrameRx[11];
            R[21] = FrameRx[12];
            tmp_rx = (R[20]<<8)|R[21];
            if (tmp_rx > 200)
                tmp_rx = 200;
            if (tmp_rx < 10)
                tmp_rx = 10;
            v_Timer[3].SetValue = tmp_rx;
            v_Timer[3].Timer_Count = tmp_rx;
            v_Timer[3].Timer_Count /= 5;

            R[20] = tmp_rx>>8;
            R[21] = tmp_rx&0x00ff;
            Flash_rw(F_R+(unsigned int)(20), R[20]);
            Flash_rw(F_R+(unsigned int)(21), R[21]);

            FrameTx[9] = 0x22;
            FrameTx[10] = 0x06;
            FrameTx[11] = R[20];
            FrameTx[12] = R[21];
            ProcessReturn();
        }
        if (aux_value == 0x10)//Limite Lacre
        {
            R[22] = FrameRx[11];
            R[23] = FrameRx[12];
            tmp_rx = (R[22]<<8)|R[23];
            if (tmp_rx > 3000)
                tmp_rx = 3000;
            if (tmp_rx < 10)
                tmp_rx = 10;
            v_Stepper[1].SetTotalValue = tmp_rx;
            v_Stepper[1].Timer_Total_Count = tmp_rx;
            v_Stepper[1].Timer_Total_Count /= 5;
            v_Timer[4].SetValue = tmp_rx;
            v_Timer[4].Timer_Count = tmp_rx;
            v_Timer[4].Timer_Count /= 5;

            R[22] = tmp_rx>>8;
            R[23] = tmp_rx&0x00ff;
            Flash_rw(F_R+(unsigned int)(22), R[22]);
            Flash_rw(F_R+(unsigned int)(23), R[23]);

            FrameTx[9] = 0x22;
            FrameTx[10] = 0x07;
            FrameTx[11] = R[22];
            FrameTx[12] = R[23];
            ProcessReturn();
        }
        if (aux_value == 0x11)//Limite Selo
        {
            R[24] = FrameRx[11];
            R[25] = FrameRx[12];
            tmp_rx = (R[24]<<8)|R[25];
            if (tmp_rx > 3000)
                tmp_rx = 3000;
            if (tmp_rx < 10)
                tmp_rx = 10;
            v_Stepper[2].SetTotalValue = tmp_rx;
            v_Stepper[2].Timer_Total_Count = tmp_rx;
            v_Stepper[2].Timer_Total_Count /= 5;
            v_Timer[5].SetValue = tmp_rx;
            v_Timer[5].Timer_Count = tmp_rx;
            v_Timer[5].Timer_Count /= 5;

            R[24] = tmp_rx>>8;
            R[25] = tmp_rx&0x00ff;
            Flash_rw(F_R+(unsigned int)(24), R[24]);
            Flash_rw(F_R+(unsigned int)(25), R[25]);

            FrameTx[9] = 0x22;
            FrameTx[10] = 0x08;
            FrameTx[11] = R[24];
            FrameTx[12] = R[25];
            ProcessReturn();
        }
        if (aux_value == 0x12)//Limite Faca
        {
            R[26] = FrameRx[11];
            R[27] = FrameRx[12];
            tmp_rx = (R[26]<<8)|R[27];
            if (tmp_rx > 3000)
                tmp_rx = 3000;
            if (tmp_rx < 10)
                tmp_rx = 10;
            v_Stepper[3].SetTotalValue = tmp_rx;
            v_Stepper[3].Timer_Total_Count = tmp_rx;
            v_Stepper[3].Timer_Total_Count /= 5;
            v_Timer[6].SetValue = tmp_rx;
            v_Timer[6].Timer_Count = tmp_rx;
            v_Timer[6].Timer_Count /= 5;

            R[26] = tmp_rx>>8;
            R[27] = tmp_rx&0x00ff;
            Flash_rw(F_R+(unsigned int)(26), R[26]);
            Flash_rw(F_R+(unsigned int)(27), R[27]);

            FrameTx[9] = 0x22;
            FrameTx[10] = 0x09;
            FrameTx[11] = R[26];
            FrameTx[12] = R[27];
            ProcessReturn();
        }

        //copy_B2X(SEG_D);
//        if (aux_value <= 0x01)
//            ProcessGetSpeed();
        break;
    case VECTOR_TIMER:

        aux1 = F_TVALUE+(unsigned int)((aux_value-1)*2);
        aux2 = FrameRx[FRAME_CMD_2+REQ_WRITE_SINGLE_REGISTER_VH];
        aux3 = F_TVALUE+(unsigned int)((aux_value-1)*2+1);
        aux4 = FrameRx[FRAME_CMD_2+REQ_WRITE_SINGLE_REGISTER_VL];
        Flash_rw(F_TVALUE+(unsigned int)((aux_value-1)*2), FrameRx[FRAME_CMD_2+REQ_WRITE_SINGLE_REGISTER_VH]);
        Flash_rw(F_TVALUE+(unsigned int)((aux_value-1)*2+1), FrameRx[FRAME_CMD_2+REQ_WRITE_SINGLE_REGISTER_VL]);

//        aux_value = (FrameRx[FRAME_CMD_2+REQ_WRITE_SINGLE_REGISTER_VH]<<8)|FrameRx[FRAME_CMD_2+REQ_WRITE_SINGLE_REGISTER_VL];

        aux_value = (*(F_TVALUE+(unsigned int)((aux_value-1)*2))<<8)|*(F_TVALUE+(unsigned int)((aux_value-1)*2+1));
        v_Timer[FrameRx[FRAME_CMD_2+REQ_WRITE_SINGLE_REGISTER_AL]].SetValue = aux_value;
        v_Timer[FrameRx[FRAME_CMD_2+REQ_WRITE_SINGLE_REGISTER_AL]].Timer_Count = aux_value/5;
//      ProcessWriteSingleRegisterTimer();
        break;
    }
}

void ProcessWriteSingleRegisterTimer()
{
    switch (FrameRx[FRAME_CMD_2+REQ_WRITE_SINGLE_REGISTER_AH]&0x0F)
        {
        case REGISTER_BASE_TIMER:
            v_Timer[FrameRx[FRAME_CMD_2+REQ_WRITE_SINGLE_REGISTER_AL]].Base = FrameRx[FRAME_CMD_2+REQ_WRITE_SINGLE_REGISTER_VL];
            break;
        case REGISTER_VALUE_TIMER:
            v_Timer[FrameRx[FRAME_CMD_2+REQ_WRITE_SINGLE_REGISTER_AL]].SetValue = (FrameRx[FRAME_CMD_2+REQ_WRITE_SINGLE_REGISTER_VH]<<8)+FrameRx[FRAME_CMD_2+REQ_WRITE_SINGLE_REGISTER_VL];
            v_Timer[FrameRx[FRAME_CMD_2+REQ_WRITE_SINGLE_REGISTER_AL]].Timer_Count = (v_Timer[FrameRx[FRAME_CMD_2+REQ_WRITE_SINGLE_REGISTER_AL]].Base == TIMER_BASE_MS1 ? v_Timer[FrameRx[FRAME_CMD_2+REQ_WRITE_SINGLE_REGISTER_AL]].SetValue : v_Timer[FrameRx[FRAME_CMD_2+REQ_WRITE_SINGLE_REGISTER_AL]].Base == TIMER_BASE_MS10 ? 10*v_Timer[FrameRx[FRAME_CMD_2+REQ_WRITE_SINGLE_REGISTER_AL]].SetValue : v_Timer[FrameRx[FRAME_CMD_2+REQ_WRITE_SINGLE_REGISTER_AL]].Base == TIMER_BASE_MS100 ? 100*v_Timer[FrameRx[FRAME_CMD_2+REQ_WRITE_SINGLE_REGISTER_AL]].SetValue : v_Timer[FrameRx[FRAME_CMD_2+REQ_WRITE_SINGLE_REGISTER_AL]].Base == TIMER_BASE_SEC ? 1000*v_Timer[FrameRx[FRAME_CMD_2+REQ_WRITE_SINGLE_REGISTER_AL]].SetValue : v_Timer[FrameRx[FRAME_CMD_2+REQ_WRITE_SINGLE_REGISTER_AL]].Base == TIMER_BASE_MIN ? 600*v_Timer[FrameRx[FRAME_CMD_2+REQ_WRITE_SINGLE_REGISTER_AL]].SetValue : 0);
            v_Timer[FrameRx[FRAME_CMD_2+REQ_WRITE_SINGLE_REGISTER_AL]].Timer_Count /= 5;
            break;
        }
}


void configureUart(uint32_t clock, uint32_t baud)
{

    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOX);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_UARTX);
    GPIOPinConfigure(GPIO_PIN_UXRX);
    GPIOPinConfigure(GPIO_PIN_UXTX);
    GPIOPinTypeUART(GPIO_PORT_UARTX, GPIO_PIN_0 | GPIO_PIN_1);

    UARTConfigSetExpClk(UARTX_BASE, clock, baud,
                                        (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE |
                                        UART_CONFIG_PAR_NONE));
    IntEnable(INT_UARTX);
    UARTIntEnable(UARTX_BASE, UART_INT_RX | UART_INT_RT);
}

void Flash_wb(uint32_t ui32Address, char byte)
{
    IntMasterDisable();
    //Prepara para gravar um word
    char vectorWrite[4];
    char *point;
    char i, j = 0;

    //Seta o endereço para gravação correto (múltiplo de 4)
    while(ui32Address % 4 != 0)
    {
        ui32Address--;
        j++;
    }

    point = ui32Address;
    for(i=0; i<4; i++)
    {
        if(i == j)
            vectorWrite[i] = byte;
        else
            vectorWrite[i] = (char) point[i];

    }
    FlashProgram(vectorWrite, (uint32_t) ui32Address, (uint32_t) 4);
    IntMasterEnable();
}


void Flash_rw(uint32_t segmento, char byte)
{
    IntMasterDisable();
    //Verifica o múltiplo de 4
    if(FLASH_TAM %4 != 0) goto retorna;

    uint32_t i=0, j=0, aux = 0;
    char vetor[256];
    char *point, *point2;

    //Adequa o endereço para 16kB
    while(segmento % 16384 != 0)
    {
        segmento = segmento - 1;
        j++;
    }

    //Verifica se o offset de memória
    if(j > FLASH_TAM) goto retorna;

    //Realiza troca para espaço de transferência
    uint32_t enderecoTroca = 0xC000;
    point = segmento;
    FlashErase((uint32_t) enderecoTroca);
    FlashProgram(point, (uint32_t) enderecoTroca, (uint32_t) FLASH_TAM);

    //Apaga e regrava
    FlashErase((uint32_t) segmento);
    point = enderecoTroca;
    i = 0;
    for(i=0; i<FLASH_TAM; i=i+256)
    {
        point = enderecoTroca + i;
        for(aux=0; aux<256; aux++)
        {
            if((aux+i) != j)
                vetor[aux] = point[aux];
            else
                vetor[aux] = byte;
        }
        FlashProgram(vetor, (uint32_t) segmento+i, (uint32_t) 256);
    }

retorna:
    IntMasterEnable();
    return;

}

void clear_Seg(uint32_t segmento)
{
    Flash_rw(segmento, 0xFF);
}

void Clear_SegX(uint32_t Segment)
{
    uint16_t i;
    for (i = 0; i < 64; i++)
        clear_Seg(Segment+i);
}

void sendTx(unsigned char size)
{
    contFrame = 0;
    while (contFrame <= size)
    {
        UARTCharPutNonBlocking(UARTX_BASE, FrameTx[contFrame]);
        contFrame = contFrame + 1;
    }
}

unsigned char checksum(unsigned char *frame, unsigned char length)
{
    unsigned char sum = 0;
    int aux_i = 0;

    for (aux_i = 0; aux_i < length-1; aux_i++)
    {
        sum += frame[aux_i];
    }

    return (0xff - sum);
}

int main(void)
{
    //Configura para utilizar o Clock Interno
    uint32_t clock;

    //UTILIZAR PARA CLOCK=120MHZ
//    clock = SysCtlClockFreqSet((SYSCTL_XTAL_25MHZ |
//                        SYSCTL_OSC_MAIN |
//                        /*SYSCTL_OSC_INT |*/
//                        SYSCTL_USE_PLL |
//                       SYSCTL_CFG_VCO_480), 120000000);

    //UTILIZAR PARA CLOCK=16MHZ
    clock = SysCtlClockFreqSet((SYSCTL_OSC_INT | SYSCTL_USE_OSC | SYSCTL_MAIN_OSC_DIS), 16000000);

    //Habilita os periféricos GPIOs
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOC);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOK);
    //SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOL);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOM);
    //SysCtlPeripheralEnable(SYSCTL_PERIPH_GPION);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOP);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOQ);

//    configureUart(clock, 115200);
    configureUart(clock, 921600);


    PDIR(PQ, LDAC);
//    DAC_Init(clock);


    //CONFIGURAÇÃO DE TESTE (J,K,L -> ENTRADA; M, N, P -> SAÍDAS)
    //Configura os pinos de entrada
    PDIR_IN(PE, (E1|E2|E3|E8|E9));
    PDIR_IN(PD, (E4|E5|E6|E7));
    PDIR_IN(PB, (E10));

    //Configura os pinos de saída
    PDIR(PM, (S1|S2|S3|S4|S5|S6|S7|S8));
    PDIR(PP, (S9|S10));
    PDIR(PC, (STATUS|ERRO));

    //Configura os pinos de saída para estado inicial = 0
    POUT(PM, S1, S1);
    POUT(PM, S2, S2);
    POUT(PM, S3, S3);
    POUT(PM, S4, S4);
    POUT(PM, S5, S5);
    POUT(PM, S6, S6);
    POUT(PM, S7, S7);
    POUT(PM, S8, S8);
    POUT(PP, S9, S9);
    POUT(PP, S10, S10);
//    GPIOPinWrite(GPIO_PORTC_BASE, GPIO_PIN_6, 0x00);

//    GPIOPadConfigSet(GPIO_PORTE_BASE, GPIO_PIN_1|GPIO_PIN_0, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD_WPU);
//    GPIOIntEnable(GPIO_PORTE_BASE, GPIO_INT_PIN_1|GPIO_INT_PIN_0);
//    GPIOIntTypeSet(GPIO_PORTE_BASE, GPIO_PIN_1, GPIO_FALLING_EDGE);
//    GPIOIntTypeSet(GPIO_PORTE_BASE, GPIO_PIN_0, GPIO_FALLING_EDGE);
//    IntPrioritySet(INT_GPIOE, 0);
//    IntRegister(INT_GPIOE, GPIOEIntHandler);
//    IntEnable(INT_GPIOE);
//    IntMasterEnable();
//
//    SysCtlDelay(100);

    Start();

//    uint8_t tmp_a, tmp_b;
//    tmp_a = R[8];
//    tmp_b = R[9];
//    Flash_wb(F_R+(unsigned int)(8), 4);
//    Flash_wb(F_R+(unsigned int)(9), 200);
//
//    tmp_a = R[8];
//    tmp_b = R[9];


    IntEnable(INT_UARTX);
    X[16] = 0;

    //resgata os valores dos inversores seta e espera 2 segundos para iniciar.
//    SysCtlDelay(16000000u / 3u);

//    pong();

    if (RoutineStarted == true)
    {

//        atualizasaidas(3, 1);
//        atualizasaidas(2, 1);
//        OperationCycle();

        SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER0);
        TimerConfigure(TIMER0_BASE, TIMER_CFG_PERIODIC);
        uint32_t tempoTimer = (clock/1000000)*LOOP_CYCLE_US; //Tempo para 20us
        TimerLoadSet(TIMER0_BASE, TIMER_A, tempoTimer);
        IntEnable(INT_TIMER0A);
        TimerIntEnable(TIMER0_BASE, TIMER_TIMA_TIMEOUT);


        IntMasterEnable();
        TimerEnable(TIMER0_BASE, TIMER_A);


//        DAC_Write(0, velocidade_esteira);
//        DAC_Write(1, velocidade_filme);
        while(1)
        {
            tmp_timer1 = count_us;
            ManageTimer();

            if (is_us_loop)
            {
                OperationCycle();
                if (index_tempos < 100/* && M[1] == 1*/)
                    tempos[index_tempos++] = count_us-last_count_us;
                is_us_loop = false;
            }

            tmp_timer2 = count_us - tmp_timer1;
            if (tmp_timer2 > tmp_timer3)
                tmp_timer3 = tmp_timer2;

        }
    }

}

void ManageTimer()
{

    if (last_count_us < count_us)
    {
        is_us_loop = true;

        if(aux_count_us >= (1000/LOOP_CYCLE_US))
        {
            count_ms++;
//            inc_cont_geral++;
//            cont_filme++;
//            cont_esteira++;
            if(count_ms%1 == 0)
            {
                is_5ms_loop = true;
                Counter_ms++;
            }
            aux_count_us = 0;

            if (count_ms % 500 == 0)//Pisca a cada 1s
            {
                estado = !estado;
                POUT(PC, STATUS, estado ? STATUS : 0x00);
            }
        }
        if (is_5ms_loop)
        {
            last_count_ms = count_ms;
        }
        last_count_us = count_us;
    }
}

void Timer0IntHandler(void)
{
    // Clear the timer interrupt.
    TimerIntClear(TIMER0_BASE, TIMER_TIMA_TIMEOUT);

    //P2OUT ^= STATUS;                            // Toggle P1.0
//    if(estado) estado = false;
//    else estado = true;

//    POUT(PC, STATUS, estado ? 0x00 : STATUS);
//    POUT(PQ, STATUS, STATUS);

    aux_count_us++;
//    if(aux_count_us<500)
//    {
//        IntDisable(INT_UART0);
//    }
//    else
//    {
//        IntEnable(INT_UART0);
//        if(aux_count_us >= 1000)
//        {
//            count_ms++;
//            if(count_ms%5 == 0)
//            {
//                aux_count_us = 0;
//                is_5ms_loop = true;
//                Counter_ms++;
//            }
//
//        }
//
//    }

    count_us++;
    //
    // Update the interrupt status.
    //
    IntMasterDisable();
    IntMasterEnable();
}

//Interrupção de UART RX
void UARTIntHandler(void)
{

    uint32_t ui32Status;
    //
    // Get the interrrupt status.
    //
    ui32Status = UARTIntStatus(UARTX_BASE, false);

    //
    // Clear the asserted interrupts.
    //
    UARTIntClear(UARTX_BASE, ui32Status);

    //
    // Loop while there are characters in the receive FIFO.
    //
    while(UARTCharsAvail(UARTX_BASE))
    {
        //
        // Read the next character from the UART and write it back to the UART.
        //

        if (contFrame == 0)
            FrameRx[contFrame] = UARTCharGetNonBlocking(UARTX_BASE);
        if (contFrame >= 1 && contFrame <= 4)
            FrameRx[contFrame] = UARTCharGetNonBlocking(UARTX_BASE);
        if (contFrame == 5)
            FrameRx[contFrame] = UARTCharGetNonBlocking(UARTX_BASE);
        if (contFrame > 5 && contFrame < FrameRx[5])
            FrameRx[contFrame] = UARTCharGetNonBlocking(UARTX_BASE);

        contFrame++;
        if (contFrame == FrameRx[5])
        {
            isFrameReady = true;
            contFrame = 0;
        }
        if (FrameRx[0] != 0x7e)
            contFrame = 0;
    }
}

void CodigoTeste()
{
    SetNewCoilInstruction(CONTACT_VECTOR_I, 0x01, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_Q, 0x01, COIL_C, 0);
    SetNewCoilInstruction(CONTACT_VECTOR_I, 0x02, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_Q, 0x02, COIL_C, 0);
    SetNewCoilInstruction(CONTACT_VECTOR_I, 0x03, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_Q, 0x03, COIL_C, 0);
    SetNewCoilInstruction(CONTACT_VECTOR_I, 0x04, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_Q, 0x05, COIL_C, 0);
    SetNewCoilInstruction(CONTACT_VECTOR_I, 0x05, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_Q, 0x04, COIL_C, 0);
    SetNewCoilInstruction(CONTACT_VECTOR_I, 0x06, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_Q, 0x06, COIL_C, 0);
    SetNewCoilInstruction(CONTACT_VECTOR_I, 0x07, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_Q, 0x07, COIL_C, 0);
    SetNewCoilInstruction(CONTACT_VECTOR_I, 0x08, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_Q, 0x08, COIL_C, 0);
    SetNewCoilInstruction(CONTACT_VECTOR_I, 0x09, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_Q, 0x09, COIL_C, 0);
    SetNewCoilInstruction(CONTACT_VECTOR_I, 0x0A, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_Q, 0x0A, COIL_C, 0);

}

void CodigoLadder()
{
    //Habilita operação da máquina
    SetNewCoilInstruction(CONTACT_VECTOR_I, 0x05, true,//se a emergencia não estiver ativa
//                          CONTACT_VECTOR_M, 0x0A, false,//o habilita geral estiver ativo (invertido)
                          CONTACT_VECTOR_I, 0x06, true,//o contator estiver fechado
                          CONTACT_VECTOR_X, 0x05, false,//e não tiver parado por falha de sensor
                          CONTACT_VECTOR_M, 0x0B, COIL_C, 0);//cria a flag M0B para habilitar a operação geral da máquina




    SetNewCoilInstruction(CONTACT_VECTOR_M, 0x0B, true,//se todas as condições iniciais para operação estiverem ok
                          CONTACT_VECTOR_Y, 0x10, true,//o habilita sensor de garrafão estiver ativo ??? era true
                          CONTACT_VECTOR_I, 0x01, false,//e o sensor do garrafão identificar um objeto
                          CONTACT_VECTOR_M, 0x05, COIL_C, 0);//cria uma flag auxiliar M0D para espelhar a operação da entrada I01 no caso válido



    SetNewCoilInstruction(CONTACT_VECTOR_M, 0x05, true,//se a emergencia não estiver ativa
//                          CONTACT_VECTOR_M, 0x0A, false,//o habilita geral estiver ativo (invertido)
                          CONTACT_VECTOR_T, 0x0A, true,//o contator estiver fechado
                          CONTACT_VECTOR_ZERO, 0x00, true,//e não tiver parado por falha de sensor
                          CONTACT_VECTOR_M, 0x0D, COIL_C, 0);//cria a flag M0B para habilitar a operação geral da máquina




    SetNewCoilInstruction(CONTACT_VECTOR_M, 0x0D, false,//se o sensor do garrafão no caso válido estiver sem objetos
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_M, 0x06, COIL_S, 0);//habilita a leitura de 1 objeto para o acionamento do lacre

    SetNewCoilInstruction(CONTACT_VECTOR_M, 0x0D, false,//se o sensor do garrafão no caso válido estiver sem objetos
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_M, 0x07, COIL_S, 0);//habilita a leitura de 1 objeto para o acionamento do selo

    SetNewCoilInstruction(CONTACT_VECTOR_M, 0x0D, false,//se o sensor do garrafão no caso válido estiver sem objetos
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_M, 0x08, COIL_S, 0);//habilita a leitura de 1 objeto para o acionamento da faca



    SetNewCoilInstruction(CONTACT_VECTOR_M, 0x0D, true,//caso o sensor do garrafão no caso válido indentificar algum objeto
                          CONTACT_VECTOR_M, 0x06, true,//estiver habilitada a leitura de 1 objeto para acionamento do lacre
                          CONTACT_VECTOR_M, 0x1C, false,//e a operação da faca estiver inativa
                          CONTACT_VECTOR_M, 0x1A, COIL_S, 0);//cria flag para indicar a permissão para acionamento de 1 ciclo do lacre


    SetNewCoilInstruction(CONTACT_VECTOR_M, 0x0D, true,//caso o sensor do garrafão no caso válido indentificar algum objeto
                          CONTACT_VECTOR_M, 0x07, true,//e estiver habilitada a leitura de 1 objeto para acionamento do selo
                          CONTACT_VECTOR_M, 0x1F, false,//e a operação da faca estiver inativa
                          CONTACT_VECTOR_M, 0x1B, COIL_S, 0);//cria flag para indicar a permissão para acionamento de 1 ciclo do selo

    SetNewCoilInstruction(CONTACT_VECTOR_Y, 0x12, false,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_M, 0x1B, COIL_R, 0);//cria flag para indicar a permissão para acionamento de 1 ciclo do selo

//
//    SetNewCoilInstruction(CONTACT_VECTOR_M, 0x09, true,
//                          CONTACT_VECTOR_M, 0x1F, false,//não lembro o que é ???
//                          CONTACT_VECTOR_ZERO, 0x00, true,
//                          CONTACT_VECTOR_M, 0x1B, COIL_S, 0);//cria flag para indicar a permissão para acionamento de 1 ciclo do selo

    SetNewCoilInstruction(CONTACT_VECTOR_M, 0x0A, false,//se a emergencia não estiver ativa
                          CONTACT_VECTOR_I, 0x06, true,//o contator estiver fechado
                          CONTACT_VECTOR_ZERO, 0x00, true,//e não tiver parado por falha de sensor
                          CONTACT_VECTOR_M, 0x03, COIL_S, 0);//cria a flag M0B para habilitar a operação geral da máquina

    SetNewCoilInstruction(CONTACT_VECTOR_M, 0x0A, false,//se a emergencia não estiver ativa
                          CONTACT_VECTOR_I, 0x06, true,//o contator estiver fechado
                          CONTACT_VECTOR_ZERO, 0x00, true,//e não tiver parado por falha de sensor
                          CONTACT_VECTOR_M, 0x0A, COIL_S, 0);//cria a flag M0B para habilitar a operação geral da máquina




//    SetNewCoilInstruction(CONTACT_VECTOR_M, 0x0D, true,//caso o sensor do garrafão no caso válido indentificar algum objeto
//                          CONTACT_VECTOR_M, 0x08, true,//estiver habilitada a leitura de 1 objeto para acionamento da faca
//                          CONTACT_VECTOR_M, 0x1A, false,//e a operação do lacre estiver inativo
//                          CONTACT_VECTOR_M, 0x1C, COIL_S, 0);//cria flag para indicar a permissão para acionamento de 1 ciclo da faca


//
//    //Habilita início de operação do lacre, se a faca estiver parada
//    SetNewCoilInstruction(CONTACT_VECTOR_M, 0x0E, true,//se existir permissão para acionamento de 1 ciclo
//                          CONTACT_VECTOR_M, 0x1C, false,//a operação da faca estiver inativa
//                          CONTACT_VECTOR_M, 0x1A, false,//e a operação do lacre estiver inativo
//                          CONTACT_VECTOR_M, 0x1A, COIL_P, 0);//habilita a operação do lacre

//    //Habilita início de operação da faca, se o lacre estiver parado
//    SetNewCoilInstruction(CONTACT_VECTOR_M, 0x0E, true,//se existir permissão para acionamento de 1 ciclo
//                          CONTACT_VECTOR_M, 0x1A, false,//a operação do lacre estiver inativo
//                          CONTACT_VECTOR_M, 0x1C, false,//e a operação da faca estiver inativa
//                          CONTACT_VECTOR_M, 0x1C, COIL_P, 0)//habilita a operação da faca



    SetNewCoilInstruction(CONTACT_VECTOR_M, 0x1A, true,
                          CONTACT_VECTOR_M, 0x01, false,
                          CONTACT_VECTOR_ZERO, 0x00, true,//habilitador de lacre //!!! modifiquei de false para true !!! Modifiquei de  Y01 para ZERO;
                          CONTACT_VECTOR_M, 0x01, COIL_P, 0);

    SetNewTimerInstruction(CONTACT_VECTOR_M, 0x01, true,
                           CONTACT_VECTOR_ZERO, 0x00, true,//CONTACT_VECTOR_Q, 0x05, true,
                           CONTACT_VECTOR_ZERO, 0x00, true,
                           0x01, TIMER_MODE_T1, TIMER_BASE_MS1, 80, 0, CONTACT_VECTOR_ZERO, 0x00, 0);
    SetNewCoilInstruction(CONTACT_VECTOR_T, 0x01, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_Q, 0x01, COIL_R, 0);//adicionar proteção emergência aqui???
    Stepper_Instructions[aux_Stepper_Inst++] = aux_Inst;
    SetNewStepperInstruction(CONTACT_VECTOR_T, 0x01, true,
                             CONTACT_VECTOR_ZERO, 0x00, true,
                             CONTACT_VECTOR_ZERO, 0x00, true,
                             0x01, TIMER_BASE_MS1, 420,//era 252
                             50, 700, 10,//era 1100
                             CONTACT_VECTOR_Q, 0x02, 0);//adicionar proteção emergência aqui???
    SetNewTimerInstruction(CONTACT_VECTOR_T, 0x01, true,
                           CONTACT_VECTOR_Q, 0x01, false,
                           CONTACT_VECTOR_ZERO, 0x00, true,
                           0x04, TIMER_MODE_T1, TIMER_BASE_MS1, 700, 0, CONTACT_VECTOR_ZERO, 0x00, 0);//Limite tempo do lacre - modificado - era 1100
    SetNewTimerInstruction(CONTACT_VECTOR_T, 0x01, true,//Tempo mínimo de atuação
                           CONTACT_VECTOR_ZERO, 0x00, true,
                           CONTACT_VECTOR_ZERO, 0x00, true,
                           0x07, TIMER_MODE_T1, TIMER_BASE_MS1, 100, 0, CONTACT_VECTOR_ZERO, 0x00, 0);

    SetNewCoilInstruction(CONTACT_VECTOR_T, 0x04, true,//
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_Q, 0x01, COIL_S, 0);
    SetNewCoilInstruction(CONTACT_VECTOR_T, 0x04, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_M, 0x01, COIL_R, 0);
    SetNewCoilInstruction(CONTACT_VECTOR_T, 0x04, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_M, 0x11, COIL_R, 0);
    SetNewCoilInstruction(CONTACT_VECTOR_T, 0x04, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_M, 0x1A, COIL_R, 0);
    SetNewCoilInstruction(CONTACT_VECTOR_T, 0x04, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_M, 0x06, COIL_R, 0);
    SetNewCoilInstruction(CONTACT_VECTOR_T, 0x04, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_X, 0x01, COIL_S, 0);////////////////

    SetNewCoilInstruction(CONTACT_VECTOR_I, 0x05, false,//
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_Q, 0x01, COIL_S, 0);
    SetNewCoilInstruction(CONTACT_VECTOR_I, 0x05, false,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_M, 0x01, COIL_R, 0);
    SetNewCoilInstruction(CONTACT_VECTOR_I, 0x05, false,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_M, 0x11, COIL_R, 0);
    SetNewCoilInstruction(CONTACT_VECTOR_I, 0x05, false,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_M, 0x1A, COIL_R, 0);
    SetNewCoilInstruction(CONTACT_VECTOR_I, 0x05, false,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_M, 0x06, COIL_R, 0);

    SetNewCoilInstruction(CONTACT_VECTOR_I, 0x02, false,//
                          CONTACT_VECTOR_T, 0x07, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_M, 0x11, COIL_S, 0);

    SetNewCoilInstruction(CONTACT_VECTOR_I, 0x02, true,
                          CONTACT_VECTOR_M, 0x11, true,
                          CONTACT_VECTOR_T, 0x07, true,
                          CONTACT_VECTOR_Q, 0x01, COIL_S, 0);
    SetNewCoilInstruction(CONTACT_VECTOR_I, 0x02, true,
                          CONTACT_VECTOR_M, 0x11, true,
                          CONTACT_VECTOR_T, 0x07, true,
                          CONTACT_VECTOR_M, 0x01, COIL_R, 0);
//    //Habilita início de operação da faca, se o lacre estiver parado
//    SetNewCoilInstruction(CONTACT_VECTOR_I, 0x02, true,//nao é isso
//                          CONTACT_VECTOR_M, 0x1A, false,//voltar aqui
//                          CONTACT_VECTOR_M, 0x1C, false,
//                          CONTACT_VECTOR_M, 0x1C, COIL_P, 0);
    SetNewCoilInstruction(CONTACT_VECTOR_I, 0x02, true,
                          CONTACT_VECTOR_M, 0x11, true,
                          CONTACT_VECTOR_M, 0x08, true,//estiver habilitada a leitura de 1 objeto para acionamento da faca
                          CONTACT_VECTOR_M, 0x1C, COIL_S, 0);
    SetNewCoilInstruction(CONTACT_VECTOR_I, 0x02, true,
                          CONTACT_VECTOR_M, 0x11, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_M, 0x1A, COIL_R, 0);
    SetNewCoilInstruction(CONTACT_VECTOR_I, 0x02, true,
                          CONTACT_VECTOR_M, 0x11, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_M, 0x06, COIL_R, 0);
    SetNewCoilInstruction(CONTACT_VECTOR_I, 0x02, true,
                          CONTACT_VECTOR_M, 0x11, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_M, 0x11, COIL_R, 0);

//
    //Habilita início de operação do selo, se a faca estiver parada
//    SetNewCoilInstruction(CONTACT_VECTOR_M, 0x0E, true,
//                          CONTACT_VECTOR_M, 0x1C, false,//voltar aqui
//                          CONTACT_VECTOR_M, 0x1F, false,
//                          CONTACT_VECTOR_M, 0x1B, COIL_P, 0);


    SetNewCoilInstruction(CONTACT_VECTOR_M, 0x1B, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_M, 0x1F, COIL_S, 0);


    SetNewCoilInstruction(CONTACT_VECTOR_M, 0x1B, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_M, 0x02, COIL_P, 0);
    SetNewTimerInstruction(CONTACT_VECTOR_M, 0x02, true,
                           CONTACT_VECTOR_ZERO, 0x00, true,
                           CONTACT_VECTOR_ZERO, 0x00, true,
                           0x02, TIMER_MODE_T1, TIMER_BASE_MS1, 100, 0, CONTACT_VECTOR_ZERO, 0x00, 0);//era 250
    SetNewCoilInstruction(CONTACT_VECTOR_T, 0x02, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_Q, 0x03, COIL_R, 0);
    Stepper_Instructions[aux_Stepper_Inst++] = aux_Inst;
    SetNewStepperInstruction(CONTACT_VECTOR_T, 0x02, true,
                             CONTACT_VECTOR_ZERO, 0x00, true,
                             CONTACT_VECTOR_ZERO, 0x00, true,
                             0x02, TIMER_BASE_MS1, 480,//era 276
                             50, 400, 10,
                             CONTACT_VECTOR_Q, 0x04, 0);
    SetNewTimerInstruction(CONTACT_VECTOR_T, 0x02, true,
                           CONTACT_VECTOR_Q, 0x03, false,
                           CONTACT_VECTOR_ZERO, 0x00, true,
                           0x05, TIMER_MODE_T1, TIMER_BASE_MS1, 400, 0, CONTACT_VECTOR_ZERO, 0x00, 0);
    SetNewTimerInstruction(CONTACT_VECTOR_T, 0x02, true,//Tempo mínimo de atuação
                           CONTACT_VECTOR_ZERO, 0x00, true,
                           CONTACT_VECTOR_ZERO, 0x00, true,
                           0x08, TIMER_MODE_T1, TIMER_BASE_MS1, 100, 0, CONTACT_VECTOR_ZERO, 0x00, 0);
    SetNewCoilInstruction(CONTACT_VECTOR_T, 0x05, true,//
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_Q, 0x03, COIL_S, 0);
    SetNewCoilInstruction(CONTACT_VECTOR_T, 0x05, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_M, 0x02, COIL_R, 0);
    SetNewCoilInstruction(CONTACT_VECTOR_T, 0x05, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_M, 0x12, COIL_R, 0);
    SetNewCoilInstruction(CONTACT_VECTOR_T, 0x05, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_M, 0x1B, COIL_R, 0);
    SetNewCoilInstruction(CONTACT_VECTOR_T, 0x05, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_M, 0x07, COIL_R, 0);
    SetNewCoilInstruction(CONTACT_VECTOR_T, 0x05, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_M, 0x09, COIL_R, 0);
    SetNewCoilInstruction(CONTACT_VECTOR_T, 0x05, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_X, 0x02, COIL_S, 0);/////////////////////

    SetNewCoilInstruction(CONTACT_VECTOR_I, 0x05, false,//
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_Q, 0x03, COIL_S, 0);
    SetNewCoilInstruction(CONTACT_VECTOR_I, 0x05, false,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_M, 0x02, COIL_R, 0);
    SetNewCoilInstruction(CONTACT_VECTOR_I, 0x05, false,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_M, 0x12, COIL_R, 0);
    SetNewCoilInstruction(CONTACT_VECTOR_I, 0x05, false,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_M, 0x1B, COIL_R, 0);
    SetNewCoilInstruction(CONTACT_VECTOR_I, 0x05, false,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_M, 0x07, COIL_R, 0);
    SetNewCoilInstruction(CONTACT_VECTOR_I, 0x05, false,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_M, 0x09, COIL_R, 0);

    SetNewCoilInstruction(CONTACT_VECTOR_I, 0x03, false,//
                          CONTACT_VECTOR_T, 0x08, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_M, 0x12, COIL_S, 0);

    SetNewCoilInstruction(CONTACT_VECTOR_I, 0x03, true,
                          CONTACT_VECTOR_M, 0x12, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_Q, 0x03, COIL_S, 0);
    SetNewCoilInstruction(CONTACT_VECTOR_I, 0x03, true,
                          CONTACT_VECTOR_M, 0x12, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_M, 0x02, COIL_R, 0);
    SetNewCoilInstruction(CONTACT_VECTOR_I, 0x03, true,
                          CONTACT_VECTOR_M, 0x12, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_M, 0x1B, COIL_R, 0);
    SetNewCoilInstruction(CONTACT_VECTOR_I, 0x03, true,
                          CONTACT_VECTOR_M, 0x12, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_M, 0x07, COIL_R, 0);
    SetNewCoilInstruction(CONTACT_VECTOR_I, 0x03, true,
                          CONTACT_VECTOR_M, 0x12, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_M, 0x09, COIL_R, 0);
//    SetNewCoilInstruction(CONTACT_VECTOR_I, 0x03, true,
//                          CONTACT_VECTOR_M, 0x1F, false,
//                          CONTACT_VECTOR_ZERO, 0x00, true,
//                          CONTACT_VECTOR_M, 0x1B, COIL_R, 0);
    SetNewCoilInstruction(CONTACT_VECTOR_I, 0x07, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_M, 0x1F, COIL_R, 0);

    SetNewCoilInstruction(CONTACT_VECTOR_I, 0x03, true,
                          CONTACT_VECTOR_M, 0x12, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_M, 0x12, COIL_R, 0);



    SetNewCoilInstruction(CONTACT_VECTOR_M, 0x1C, true,
                          CONTACT_VECTOR_Y, 0x10, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_M, 0x03, COIL_P, 0);

    SetNewTimerInstruction(CONTACT_VECTOR_M, 0x03, true,
                           CONTACT_VECTOR_ZERO, 0x00, true,
                           CONTACT_VECTOR_ZERO, 0x00, true,
                           0x03, TIMER_MODE_T1, TIMER_BASE_MS1, 10, 0, CONTACT_VECTOR_ZERO, 0x00, 0);
    SetNewCoilInstruction(CONTACT_VECTOR_T, 0x03, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_Q, 0x05, COIL_R, 0);
    Stepper_Instructions[aux_Stepper_Inst++] = aux_Inst;
    SetNewStepperInstruction(CONTACT_VECTOR_T, 0x03, true,
                             CONTACT_VECTOR_ZERO, 0x00, true,
                             CONTACT_VECTOR_ZERO, 0x00, true,
                             0x03, TIMER_BASE_MS1, 2400,
                             50, 2200, 10,
                             CONTACT_VECTOR_Q, 0x06, 0);
    SetNewTimerInstruction(CONTACT_VECTOR_T, 0x03, true,
                           CONTACT_VECTOR_Q, 0x05, false,
                           CONTACT_VECTOR_ZERO, 0x00, true,
                           0x06, TIMER_MODE_T1, TIMER_BASE_MS1, 2200, 0, CONTACT_VECTOR_ZERO, 0x00, 0);
    SetNewTimerInstruction(CONTACT_VECTOR_T, 0x03, true,//Tempo mínimo de atuação
                           CONTACT_VECTOR_Q, 0x05, false,
                           CONTACT_VECTOR_ZERO, 0x00, true,
                           0x09, TIMER_MODE_T1, TIMER_BASE_MS1, 100, 0, CONTACT_VECTOR_ZERO, 0x00, 0);
    SetNewCoilInstruction(CONTACT_VECTOR_T, 0x06, true,//
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_Q, 0x05, COIL_S, 0);
    SetNewCoilInstruction(CONTACT_VECTOR_T, 0x06, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_M, 0x03, COIL_R, 0);
    SetNewCoilInstruction(CONTACT_VECTOR_T, 0x06, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_M, 0x13, COIL_R, 0);
    SetNewCoilInstruction(CONTACT_VECTOR_T, 0x06, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_M, 0x1C, COIL_R, 0);
    SetNewCoilInstruction(CONTACT_VECTOR_T, 0x06, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_M, 0x08, COIL_R, 0);
    SetNewCoilInstruction(CONTACT_VECTOR_T, 0x06, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_X, 0x03, COIL_S, 0);//////////////////////
//



    SetNewCoilInstruction(CONTACT_VECTOR_I, 0x05, false,//
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_Q, 0x05, COIL_S, 0);
    SetNewCoilInstruction(CONTACT_VECTOR_I, 0x05, false,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_M, 0x03, COIL_R, 0);
    SetNewCoilInstruction(CONTACT_VECTOR_I, 0x05, false,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_M, 0x13, COIL_R, 0);
    SetNewCoilInstruction(CONTACT_VECTOR_I, 0x05, false,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_M, 0x1C, COIL_R, 0);
    SetNewCoilInstruction(CONTACT_VECTOR_I, 0x05, false,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_M, 0x08, COIL_R, 0);



    SetNewCoilInstruction(CONTACT_VECTOR_I, 0x07, true,//
                          CONTACT_VECTOR_T, 0x09, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_M, 0x13, COIL_S, 0);
    SetNewCoilInstruction(CONTACT_VECTOR_I, 0x07, false,
                          CONTACT_VECTOR_M, 0x13, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_Q, 0x05, COIL_S, 0);
    SetNewCoilInstruction(CONTACT_VECTOR_I, 0x07, false,
                          CONTACT_VECTOR_M, 0x13, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_M, 0x03, COIL_R, 0);
    SetNewCoilInstruction(CONTACT_VECTOR_I, 0x07, false,
                          CONTACT_VECTOR_M, 0x13, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_M, 0x1C, COIL_R, 0);
    SetNewCoilInstruction(CONTACT_VECTOR_I, 0x07, false,
                          CONTACT_VECTOR_M, 0x13, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_M, 0x08, COIL_R, 0);
    SetNewCoilInstruction(CONTACT_VECTOR_I, 0x07, false,
                          CONTACT_VECTOR_M, 0x13, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_M, 0x13, COIL_R, 0);

    SetNewCoilInstruction(CONTACT_VECTOR_M, 0x1D, true,
                          CONTACT_VECTOR_I, 0x05, true,
                          CONTACT_VECTOR_Y, 0x10, false,
                          CONTACT_VECTOR_Q, 0x07, COIL_C, 0);

    SetNewCoilInstruction(CONTACT_VECTOR_M, 0x1E, true,
                          CONTACT_VECTOR_I, 0x05, true,
                          CONTACT_VECTOR_Y, 0x10, false,
                          CONTACT_VECTOR_Q, 0x08, COIL_C, 0);

    SetNewCoilInstruction(CONTACT_VECTOR_M, 0x0B, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,//!!!modifiquei de false para true.!!!Desfiz y11 para zero
                          CONTACT_VECTOR_I, 0x05, true,
                          CONTACT_VECTOR_Q, 0x09, COIL_C, 0);

    SetNewCoilInstruction(CONTACT_VECTOR_M, 0x0B, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,//!!!modifiquei de false para true.!!!Desfiz y11 para zero
                          CONTACT_VECTOR_I, 0x05, true,
                          CONTACT_VECTOR_Q, 0x0A, COIL_C, 0);

//    SetNewCoilInstruction(CONTACT_VECTOR_M, 0x0F, false,
//                          CONTACT_VECTOR_ZERO, 0x00, true,
//                          CONTACT_VECTOR_ZERO, 0x00, true,
//                          CONTACT_VECTOR_M, 0x1C, COIL_S, 0);
//    SetNewCoilInstruction(CONTACT_VECTOR_M, 0x0F, false,
//                          CONTACT_VECTOR_ZERO, 0x00, true,
//                          CONTACT_VECTOR_ZERO, 0x00, true,
//                          CONTACT_VECTOR_M, 0x03, COIL_S, 0);
//    SetNewCoilInstruction(CONTACT_VECTOR_M, 0x0F, false,
//                          CONTACT_VECTOR_ZERO, 0x00, true,
//                          CONTACT_VECTOR_ZERO, 0x00, true,
//                          CONTACT_VECTOR_M, 0x0F, COIL_S, 0);

    SetNewCoilInstruction(CONTACT_VECTOR_X, 0x01, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_X, 0x05, COIL_S, 0);

    SetNewCoilInstruction(CONTACT_VECTOR_X, 0x02, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_X, 0x05, COIL_S, 0);

    SetNewCoilInstruction(CONTACT_VECTOR_X, 0x03, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_X, 0x05, COIL_S, 0);

    SetNewCoilInstruction(CONTACT_VECTOR_X, 0x01, true,
                          CONTACT_VECTOR_I, 0x06, false,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_X, 0x01, COIL_R, 0);
    SetNewCoilInstruction(CONTACT_VECTOR_X, 0x02, true,
                          CONTACT_VECTOR_I, 0x06, false,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_X, 0x02, COIL_R, 0);
    SetNewCoilInstruction(CONTACT_VECTOR_X, 0x03, true,
                          CONTACT_VECTOR_I, 0x06, false,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_X, 0x03, COIL_R, 0);
    SetNewCoilInstruction(CONTACT_VECTOR_X, 0x05, true,
                          CONTACT_VECTOR_I, 0x06, false,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_X, 0x05, COIL_R, 0);

    SetNewCoilInstruction(CONTACT_VECTOR_I, 0x06, false,
                          CONTACT_VECTOR_M, 0x0A, true,
                          CONTACT_VECTOR_ZERO, 0x00, true,
                          CONTACT_VECTOR_M, 0x0A, COIL_R, 0);

//    Stepper_Instructions[aux_Stepper_Inst++] = aux_Inst;
//    SetNewStepperInstruction(CONTACT_VECTOR_ZERO, 0x00, true,
//                             CONTACT_VECTOR_ZERO, 0x00, true,
//                             CONTACT_VECTOR_ZERO, 0x00, true,
//                             0x03, TIMER_BASE_MS1, 1200,
//                             50, 0, 10,
//                             CONTACT_VECTOR_Q, 0x06, 0);



    SetNewTimerInstruction(CONTACT_VECTOR_Q, 0x09, true,
                           CONTACT_VECTOR_ZERO, 0x00, true,
                           CONTACT_VECTOR_ZERO, 0x00, true,
                           0x0A, TIMER_MODE_T1, TIMER_BASE_MS1, 1000, 0, CONTACT_VECTOR_ZERO, 0x00, 0);
}

